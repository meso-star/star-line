/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SLN_H
#define SLN_H

#include <rsys/rsys.h>

#include <float.h>
#include <math.h>

/* Library symbol management */
#if defined(SLN_SHARED_BUILD)  /* Build shared library */
  #define SLN_API extern EXPORT_SYM
#elif defined(SLN_STATIC)  /* Use/build static library */
  #define SLN_API extern LOCAL_SYM
#else
  #define SLN_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the sln function `Func'
 * returns an error. One should use this macro on sln calls for which no
 * explicit error checking is performed */
#ifndef NDEBUG
  #define SLN(Func) ASSERT(sln_ ## Func == RES_OK)
#else
  #define SLN(Func) sln_ ## Func
#endif

#define SLN_MAX_MOLECULES_COUNT 100
#define SLN_MAX_ISOTOPES_COUNT 10

/* Forward declaration of external data structures */
struct logger;
struct mem_allocator;
struct shtr;
struct shtr_line;
struct shtr_isotope_metadata;
struct shtr_line_list;

enum sln_mesh_type {
  SLN_MESH_UPPER, /* Mesh that majorize the underlying lines */
  SLN_MESH_USUAL, /* Mesh of the lines */
  SLN_MESH_TYPES_COUNT__
};

enum sln_line_profile {
  SLN_LINE_PROFILE_VOIGT,
  SLN_LINE_PROFILES_COUNT__
};

struct sln_device_create_args {
  struct logger* logger; /* May be NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define SLN_DEVICE_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct sln_device_create_args SLN_DEVICE_CREATE_ARGS_DEFAULT =
  SLN_DEVICE_CREATE_ARGS_DEFAULT__;

struct sln_isotope {
  double abundance;
  int32_t id_local; /* Isotopes identifier _local_ to the molecule */
};
#define SLN_ISOTOPE_NULL__ {0, 0}
static const struct sln_isotope SLN_ISOTOPE_NULL = SLN_ISOTOPE_NULL__;

struct sln_molecule {
  /* List of isotopes to be taken into account */
  struct sln_isotope isotopes[SLN_MAX_ISOTOPES_COUNT];
  /* 0 <=> select all the isotopes of the molecules with reference abundance */
  size_t nisotopes;

  double concentration;
  double cutoff; /* in cm^-1 */

  int32_t id; /* Identifier of the molecule into the isotope metadata */
};
#define SLN_MOLECULE_NULL__ {{SLN_ISOTOPE_NULL__}, 0, 0, 0, 0}
static const struct sln_molecule SLN_MOLECULE_NULL = SLN_MOLECULE_NULL__;

struct sln_mixture_create_args {
  /* Isotope metadata and overall list of spectral lines */
  struct shtr_isotope_metadata* metadata;
  struct shtr_line_list* lines;

  /* List of molecules to be taken into account with its associated
   * concentration and cutoff */
  struct sln_molecule molecules[SLN_MAX_MOLECULES_COUNT];
  size_t nmolecules;

  double wavenumber_range[2]; /* Spectral range */

  /* Thermodynamic properties */
  double pressure; /* In atm */
  double temperature; /* In K */
};
#define SLN_MIXTURE_CREATE_ARGS_DEFAULT__  \
  {NULL, NULL, {SLN_MOLECULE_NULL__}, 0, {0, DBL_MAX}, 0, 0}
static const struct sln_mixture_create_args SLN_MIXTURE_CREATE_ARGS_DEFAULT =
  SLN_MIXTURE_CREATE_ARGS_DEFAULT__;

struct sln_mixture_desc {
  double wavenumber_range[2]; /* Spectral range */
  double pressure; /* In atm */
  double temperature; /* In K */
  size_t nlines; /* Number of partitioned lines */
};
#define SLN_MIXTURE_DESC_NULL__ {{0,0},0,0,0}
static const struct sln_mixture_desc SLN_MIXTURE_DESC_NULL =
  SLN_MIXTURE_DESC_NULL__;

struct sln_tree_create_args {
  /* Max number of lines per leaf */
  size_t max_nlines_per_leaf;

  /* Hint on the number of vertices around the line center */
  size_t nvertices_hint;

  /* Relative error used to simplify the spectrum mesh. The larger it is, the
   * coarser the mesh */
  float mesh_decimation_err; /* > 0 */
  enum sln_mesh_type mesh_type; /* Type of mesh to generate */

  enum sln_line_profile line_profile;
};
#define SLN_TREE_CREATE_ARGS_DEFAULT__ {                                       \
  1, 16, 0.01f, SLN_MESH_UPPER, SLN_LINE_PROFILE_VOIGT                         \
}
static const struct sln_tree_create_args SLN_TREE_CREATE_ARGS_DEFAULT =
  SLN_TREE_CREATE_ARGS_DEFAULT__;

struct sln_tree_desc {
  size_t max_nlines_per_leaf; /* max #lines per leaf */
  float mesh_decimation_err;
  enum sln_mesh_type mesh_type;
  enum sln_line_profile line_profile;
};
#define SLN_TREE_DESC_NULL__ {                                                 \
  0, 0, SLN_MESH_TYPES_COUNT__, SLN_LINE_PROFILES_COUNT__                      \
}
static const struct sln_tree_desc SLN_TREE_DESC_NULL = SLN_TREE_DESC_NULL__;

struct sln_vertex { /* 8 Bytes */
  float wavenumber; /* in cm^-1 */
  float ka;
};
#define SLN_VERTEX_NULL__ {0,0}
static const struct sln_vertex SLN_VERTEX_NULL = SLN_VERTEX_NULL__;

struct sln_mesh {
  const struct sln_vertex* vertices;
  size_t nvertices;
};
#define SLN_MESH_NULL__ {NULL,0}
static const struct sln_mesh SLN_MESH_NULL = SLN_MESH_NULL__;

/* Forward declarations of opaque data structures */
struct sln_device;
struct sln_mixture;
struct sln_node;
struct sln_tree;

/*******************************************************************************
 * Device API
 ******************************************************************************/
SLN_API res_T
sln_device_create
  (const struct sln_device_create_args* args,
   struct sln_device** sln);

SLN_API res_T
sln_device_ref_get
  (struct sln_device* sln);

SLN_API res_T
sln_device_ref_put
  (struct sln_device* sln);

/*******************************************************************************
 * Mixture API
 ******************************************************************************/
SLN_API res_T
sln_mixture_create
  (struct sln_device* sln,
   const struct sln_mixture_create_args* args,
   struct sln_mixture** mixture);

/* Load a mixture serialized with the "shtr_mixture_write" function */
SLN_API res_T
sln_mixture_create_from_stream
  (struct sln_device* sln,
   struct shtr* shtr,
   FILE* stream,
   struct sln_mixture** mixture);

SLN_API res_T
sln_mixture_ref_get
  (struct sln_mixture* mixture);

SLN_API res_T
sln_mixture_ref_put
  (struct sln_mixture* mixture);

SLN_API res_T
sln_mixture_get_desc
  (const struct sln_mixture* mixture,
   struct sln_mixture_desc* desc);

SLN_API res_T
sln_mixture_write
  (const struct sln_mixture* mixture,
   FILE* stream);

/*******************************************************************************
 * Tree API
 ******************************************************************************/
SLN_API res_T
sln_tree_create
  (struct sln_mixture* mixture,
   const struct sln_tree_create_args* args,
   struct sln_tree** tree);

/* Load a tree serialized with the "sln_tree_write" function */
SLN_API res_T
sln_tree_create_from_stream
  (struct sln_device* sln,
   struct shtr* shtr,
   FILE* stream,
   struct sln_tree** tree);

SLN_API res_T
sln_tree_ref_get
  (struct sln_tree* tree);

SLN_API res_T
sln_tree_ref_put
  (struct sln_tree* tree);

SLN_API res_T
sln_tree_get_desc
  (const struct sln_tree* tree,
   struct sln_tree_desc* desc);

SLN_API const struct sln_node*
sln_tree_get_root
  (const struct sln_tree* tree);

SLN_API int
sln_node_is_leaf
  (const struct sln_node* node);

/* Return NULL if the node is a leaf */
SLN_API const struct sln_node*
sln_node_get_child
  (const struct sln_node* node,
   const unsigned ichild); /* 0 or 1 */

SLN_API size_t
sln_node_get_lines_count
  (const struct sln_node* node);

SLN_API res_T
sln_node_get_line
  (const struct sln_tree* tree,
   const struct sln_node* node,
   const size_t iline,
   const struct shtr_line** line);

SLN_API res_T
sln_node_get_mesh
  (const struct sln_tree* tree,
   const struct sln_node* node,
   struct sln_mesh* mesh);

SLN_API double
sln_node_eval
  (const struct sln_tree* tree,
   const struct sln_node* node,
   const double wavenumber); /* In cm^-1 */

SLN_API double
sln_mesh_eval
  (const struct sln_mesh* mesh,
   const double wavenumber); /* In cm^-1 */

SLN_API res_T
sln_tree_write
  (const struct sln_tree* tree,
   FILE* stream);

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
/* Purpose: to calculate the Faddeeva function with relative error less than
 * 10^(-4).
 *
 * Inputs: x and y, parameters for the Voigt function :
 * - x is defined as x=(nu-nu_c)/gamma_D*sqrt(ln(2)) with nu the current
 *   wavenumber, nu_c the wavenumber at line center, gamma_D the Doppler
 *   linewidth.
 * - y is defined as y=gamma_L/gamma_D*sqrt(ln(2)) with gamma_L the Lorentz
 *   linewith and gamma_D the Doppler linewidth
 *
 * Output: k, the Voigt function; it has to be multiplied by
 * sqrt(ln(2)/pi)*1/gamma_D so that the result may be interpretable in terms of
 * line profile.
 *
 * TODO check the copyright */
SLN_API double
sln_faddeeva
  (const double x,
   const double y);

static INLINE double
sln_compute_line_half_width_doppler
  (const double nu, /* Line center wrt pressure in cm^-1 */ /* TODO check this */
   const double molar_mass, /* In kg.mol^-1 */
   const double temperature) /* In K */
{
  /* kb = 1.3806e-23
   * Na = 6.02214076e23
   * c = 299792458
   * sqrt(2*log(2)*kb*Na)/c */
  const double sqrt_two_ln2_kb_Na_over_c = 1.1324431552553545042e-08;
  const double gamma_d = nu * sqrt_two_ln2_kb_Na_over_c * sqrt(temperature/molar_mass);
  ASSERT(nu > 0 && temperature >= 0 && molar_mass > 0);
  return gamma_d;
}

static INLINE double
sln_compute_line_half_width_lorentz
  (const double gamma_air, /* Air broadening half width [cm^-1.atm^-1] */
   const double gamma_self, /* Air broadening half width [cm^-1.atm^-1] */
   const double pressure, /* [atm^-1] */
   const double concentration)
{
  const double Ps = pressure * concentration;
  const double gamma_l = (pressure - Ps) * gamma_air + Ps * gamma_self;
  ASSERT(gamma_air > 0 && gamma_self > 0);
  ASSERT(pressure > 0 && concentration >= 0 && concentration <= 1);
  return gamma_l;
}

static INLINE double
sln_compute_voigt_profile
  (const double wavenumber, /* In cm^-1 */
   const double nu, /* Line center in cm^-1 */
   const double gamma_d, /* Doppler line half width in cm^-1 */
   const double gamma_l) /* Lorentz line half width in cm^-1 */
{
  /* Constants */
  const double sqrt_ln2 = 0.83255461115769768821; /* sqrt(log(2)) */
  const double sqrt_ln2_over_pi = 0.46971863934982566180; /* sqrt(log(2)/M_PI) */
  const double sqrt_ln2_over_gamma_d = sqrt_ln2 / gamma_d;

  const double x = (wavenumber - nu) * sqrt_ln2_over_gamma_d;
  const double y = gamma_l * sqrt_ln2_over_gamma_d;
  const double k = sln_faddeeva(x, y);
  return k*sqrt_ln2_over_pi/gamma_d;
}

#endif /* SLN_H */

/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_sln_lines.h"
#include "sln.h"

#include <star/shtr.h>
#include <rsys/math.h>
#include <rsys/mem_allocator.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
test_mixture
  (struct sln_device* sln,
   struct shtr_isotope_metadata* metadata,
   struct shtr_line_list* line_list)
{
  struct sln_mixture_create_args mixture_args = SLN_MIXTURE_CREATE_ARGS_DEFAULT;
  struct sln_mixture_desc desc = SLN_MIXTURE_DESC_NULL;
  struct sln_mixture* mixture = NULL;
  size_t nlines = 0;

  mixture_args.metadata = metadata;
  mixture_args.lines = line_list;
  mixture_args.molecules[0].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[0].concentration = 1.0/3.0;
  mixture_args.molecules[0].cutoff = 25;
  mixture_args.molecules[0].id = 1; /* H2O */
  mixture_args.molecules[1].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[1].concentration = 1.0/3.0;
  mixture_args.molecules[1].cutoff = 50;
  mixture_args.molecules[1].id = 2; /* CO2 */
  mixture_args.molecules[2].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[2].concentration = 1.0/3.0;
  mixture_args.molecules[2].cutoff = 25;
  mixture_args.molecules[2].id = 3; /* O3 */
  mixture_args.nmolecules = 3;
  mixture_args.wavenumber_range[0] = 0;
  mixture_args.wavenumber_range[1] = INF;
  mixture_args.pressure = 1;
  mixture_args.temperature = 296;
  CHK(sln_mixture_create(NULL, &mixture_args, &mixture) == RES_BAD_ARG);
  CHK(sln_mixture_create(sln, NULL, &mixture) == RES_BAD_ARG);
  CHK(sln_mixture_create(sln, NULL, NULL) == RES_BAD_ARG);
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_OK);

  CHK(sln_mixture_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(sln_mixture_get_desc(mixture, NULL) == RES_BAD_ARG);
  CHK(sln_mixture_get_desc(mixture, &desc) == RES_OK);

  CHK(shtr_line_list_get_size(line_list, &nlines) == RES_OK);

  CHK(desc.wavenumber_range[0] == mixture_args.wavenumber_range[0]);
  CHK(desc.wavenumber_range[1] == mixture_args.wavenumber_range[1]);
  CHK(desc.temperature = mixture_args.temperature);
  CHK(desc.pressure == mixture_args.pressure);
  CHK(desc.nlines == nlines); /* All the lines are taken into the count */

  CHK(sln_mixture_ref_get(NULL) == RES_BAD_ARG);
  CHK(sln_mixture_ref_get(mixture) == RES_OK);
  CHK(sln_mixture_ref_put(NULL) == RES_BAD_ARG);
  CHK(sln_mixture_ref_put(mixture) == RES_OK);
  CHK(sln_mixture_ref_put(mixture) == RES_OK);

  mixture_args.metadata = NULL;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.metadata = metadata;
  mixture_args.lines = NULL;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.metadata = metadata;
  mixture_args.lines = NULL;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.lines = line_list;
  mixture_args.molecules[0].concentration = 1;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.molecules[0].concentration = -1;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.molecules[0].concentration = 1.0/3.0;
  mixture_args.molecules[0].cutoff = 0;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.molecules[0].cutoff = 25;
  mixture_args.pressure = -1;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);

  mixture_args.pressure = 1;
  mixture_args.temperature = -1;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);
  mixture_args.temperature = 296;

  mixture_args.molecules[0].nisotopes = 2;
  mixture_args.molecules[0].isotopes[0].abundance = 0.6;
  mixture_args.molecules[0].isotopes[0].id_local = 0;
  mixture_args.molecules[0].isotopes[1].abundance = 0.5;
  mixture_args.molecules[0].isotopes[1].id_local = 1;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);
  mixture_args.molecules[0].isotopes[0].abundance = 0.5;

  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_OK);
  CHK(sln_mixture_ref_put(mixture) == RES_OK);

  mixture_args.molecules[0].id = SLN_MAX_MOLECULES_COUNT;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);
  mixture_args.molecules[0].id = 1;
  mixture_args.molecules[0].isotopes[0].id_local = SLN_MAX_ISOTOPES_COUNT;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_BAD_ARG);
}

static void
test_mixture_serialization
  (struct sln_device* sln,
   struct shtr* shtr,
   struct shtr_isotope_metadata* metadata,
   struct shtr_line_list* line_list)
{
  struct sln_mixture_create_args mixture_args = SLN_MIXTURE_CREATE_ARGS_DEFAULT;
  struct sln_mixture_desc desc1 = SLN_MIXTURE_DESC_NULL;
  struct sln_mixture_desc desc2 = SLN_MIXTURE_DESC_NULL;
  struct sln_mixture* mixture1 = NULL;
  struct sln_mixture* mixture2 = NULL;
  FILE* fp = NULL;

  mixture_args.metadata = metadata;
  mixture_args.lines = line_list;
  mixture_args.molecules[0].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[0].concentration = 1.0/3.0;
  mixture_args.molecules[0].cutoff = 25;
  mixture_args.molecules[0].id = 1; /* H2O */
  mixture_args.molecules[1].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[1].concentration = 1.0/3.0;
  mixture_args.molecules[1].cutoff = 50;
  mixture_args.molecules[1].id = 2; /* CO2 */
  mixture_args.molecules[2].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[2].concentration = 1.0/3.0;
  mixture_args.molecules[2].cutoff = 25;
  mixture_args.molecules[2].id = 3; /* O3 */
  mixture_args.nmolecules = 3;
  mixture_args.wavenumber_range[0] = 0;
  mixture_args.wavenumber_range[1] = INF;
  mixture_args.pressure = 1;
  mixture_args.temperature = 296;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture1) == RES_OK);

  CHK(fp = tmpfile());
  CHK(sln_mixture_write(NULL, fp) == RES_BAD_ARG);
  CHK(sln_mixture_write(mixture1, NULL) == RES_BAD_ARG);
  CHK(sln_mixture_write(mixture1, fp) == RES_OK);
  rewind(fp);

  CHK(sln_mixture_create_from_stream(NULL, shtr, fp, &mixture2) == RES_BAD_ARG);
  CHK(sln_mixture_create_from_stream(sln, NULL, fp, &mixture2) == RES_BAD_ARG);
  CHK(sln_mixture_create_from_stream(sln, shtr, NULL, &mixture2) == RES_BAD_ARG);
  CHK(sln_mixture_create_from_stream(sln, shtr, fp, NULL) == RES_BAD_ARG);
  CHK(sln_mixture_create_from_stream(sln, shtr, fp, &mixture2) == RES_OK);
  fclose(fp);

  CHK(sln_mixture_get_desc(mixture1, &desc1) == RES_OK);
  CHK(sln_mixture_get_desc(mixture2, &desc2) == RES_OK);

  CHK(desc1.wavenumber_range[0] == desc2.wavenumber_range[0]);
  CHK(desc1.wavenumber_range[1] == desc2.wavenumber_range[1]);
  CHK(desc1.pressure == desc2.pressure);
  CHK(desc1.temperature == desc2.temperature);
  CHK(desc1.nlines == desc2.nlines);

  CHK(sln_mixture_ref_put(mixture1) == RES_OK);
  CHK(sln_mixture_ref_put(mixture2) == RES_OK);
}

/*******************************************************************************
 * Test function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sln_device_create_args dev_args = SLN_DEVICE_CREATE_ARGS_DEFAULT;

  struct sln_device* sln = NULL;

  struct shtr_create_args shtr_args = SHTR_CREATE_ARGS_DEFAULT;
  struct shtr* shtr = NULL;
  struct shtr_line_list* line_list = NULL;
  struct shtr_isotope_metadata* metadata = NULL;

  FILE* fp_lines = NULL;
  FILE* fp_mdata = NULL;
  (void)argc, (void)argv;

  /* Generate the file of the isotope metadata */
  CHK(fp_mdata = tmpfile());
  fprintf(fp_mdata, "Molecule # Iso Abundance Q(296K) gj Molar Mass(g)\n");
  write_shtr_molecule(fp_mdata, &g_H2O);
  write_shtr_molecule(fp_mdata, &g_CO2);
  write_shtr_molecule(fp_mdata, &g_O3);
  rewind(fp_mdata);

  /* Generate the file of lines */
  CHK(fp_lines = tmpfile());
  write_shtr_lines(fp_lines, g_lines, g_nlines);
  rewind(fp_lines);

  /* Load the isotope metadata and the lines */
  shtr_args.verbose = 1;
  CHK(shtr_create(&shtr_args, &shtr) == RES_OK);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp_mdata, NULL, &metadata) == RES_OK);
  CHK(shtr_line_list_load_stream(shtr, fp_lines, NULL, &line_list) == RES_OK);

  CHK(fclose(fp_lines) == 0);
  CHK(fclose(fp_mdata) == 0);

  dev_args.verbose = 1;
  CHK(sln_device_create(&dev_args, &sln) == RES_OK);

  test_mixture(sln, metadata, line_list);
  test_mixture_serialization(sln, shtr, metadata, line_list);

  CHK(sln_device_ref_put(sln) == RES_OK);
  CHK(shtr_ref_put(shtr) == RES_OK);
  CHK(shtr_line_list_ref_put(line_list) == RES_OK);
  CHK(shtr_isotope_metadata_ref_put(metadata) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}

/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SLN_TREE_C_H
#define SLN_TREE_C_H

#include "sln.h"
#include "sln_line.h"

#include <rsys/dynamic_array.h>
#include <rsys/ref_count.h>

/* Current version of the serialized tree data. One should increment it and
 * perform a version management onto serialized tree when these data are
 * updated. */
static const int SLN_TREE_VERSION = 0;

/* Forward declaration */
struct sln_mixture;
struct sln_tree_create_args;
struct shtr_line_view;

struct sln_node { /* 32 Bytes */
  /* Range of the line indices corresponding to the node */
  uint64_t range[2];
  uint64_t ivertex; /* Index toward the 1st vertex */
  uint32_t nvertices; /* #vertices */
  uint32_t offset; /* Offset toward the node's children (left then right) */
};
#define SLN_NODE_NULL__ {{0,0},0,0,0}
static const struct sln_node SLN_NODE_NULL = SLN_NODE_NULL__;

/* Generate the dynamic array of nodes */
#define DARRAY_DATA struct sln_node
#define DARRAY_NAME node
#include <rsys/dynamic_array.h>

/* Generate the dynamic array of vertices */
#define DARRAY_DATA struct sln_vertex
#define DARRAY_NAME vertex
#include <rsys/dynamic_array.h>

struct sln_tree {
  struct darray_node nodes; /* Nodes used to partition the lines */
  struct darray_vertex vertices; /* List of vertices */

  size_t max_nlines_per_leaf;
  float mesh_decimation_err;
  enum sln_mesh_type mesh_type;
  enum sln_line_profile line_profile;

  struct sln_mixture* mixture;
  ref_T ref;
};

extern LOCAL_SYM res_T
tree_build
  (struct sln_tree* tree,
   const struct sln_tree_create_args* args);

#endif /* SLN_TREE_C_H */

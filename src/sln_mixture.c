/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sln.h"
#include "sln_device_c.h"
#include "sln_log.h"
#include "sln_mixture_c.h"

#include <star/shtr.h>
#include <rsys/cstr.h>

STATIC_ASSERT(SLN_MAX_MOLECULES_COUNT <= SHTR_MAX_MOLECULES_COUNT,
  Invalid_SLN_MAX_MOLECULES_COUNT);
STATIC_ASSERT(SLN_MAX_ISOTOPES_COUNT <= SHTR_MAX_ISOTOPES_COUNT,
  Invalid_SLN_MAX_ISOTOPES_COUNT);

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_sln_isotope
  (struct sln_device* sln,
   const char* caller,
   const int32_t mol_id,
   const size_t mol_nisos,
   const struct sln_isotope* isotope)
{
  ASSERT(sln && caller && mol_nisos && isotope);

  if(isotope->id_local >= SLN_MAX_ISOTOPES_COUNT) {
    log_err(sln,
      "%s: molecule %d: isotope %d: invalid isotope local identifier. "
      "It must be less than %d.\n",
      caller, mol_id, isotope->id_local, SLN_MAX_ISOTOPES_COUNT);
    return RES_BAD_ARG;
  }

  if((size_t)isotope->id_local >= mol_nisos) {
    log_warn(sln,
      "%s: molecule %d: the isotope local index %d exceeds "
      "the overall number of isotopes %lu.\n",
      caller, mol_id, isotope->id_local,  mol_nisos);
    return RES_OK; /* Do not check the others isotope parameters */
  }

  if(isotope->abundance < 0) {
    log_err(sln,
      "%s: molecule %d: isotope %d: invalid abundance %g.\n",
      caller, mol_id, isotope->id_local, isotope->abundance);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
check_sln_molecule
  (struct sln_device* sln,
   const char* caller,
   struct shtr_isotope_metadata* metadata,
   const struct sln_molecule* molecule)
{
  double abundances_sum = 0;
  struct shtr_molecule mol = SHTR_MOLECULE_NULL;
  size_t i;
  ASSERT(sln && caller && metadata && molecule);

  if(molecule->id >= SLN_MAX_MOLECULES_COUNT) {
    log_err(sln,
      "%s: molecule %d: invalid molecule indentifier. It must be less than %d.\n",
      caller, molecule->id, SLN_MAX_MOLECULES_COUNT);
    return RES_BAD_ARG;
  }

  SHTR(isotope_metadata_find_molecule(metadata, molecule->id, &mol));
  if(SHTR_MOLECULE_IS_NULL(&mol)) {
    log_warn(sln,
      "%s: the molecule %d is not found in the isotope metadata.\n",
      caller, molecule->id);
    return RES_OK; /* Do not check the others molecule parameters */
  }

  if(molecule->nisotopes >= SLN_MAX_ISOTOPES_COUNT) {
    log_err(sln,
      "%s: too many isotopes %lu regarding the maximum number of per molecule "
      "isotopes (SLN_MAX_ISOTOPES_COUNT = %lu).\n",
      caller,
      (unsigned long)molecule->nisotopes,
      (unsigned long)SLN_MAX_ISOTOPES_COUNT);
    return RES_BAD_ARG;
  }

  abundances_sum = 0;
  FOR_EACH(i, 0, molecule->nisotopes) {
    const res_T res = check_sln_isotope
      (sln, caller, molecule->id, molecule->nisotopes, molecule->isotopes+i);
    if(res != RES_OK) return res;
    abundances_sum += molecule->isotopes[i].abundance;
  }

  if(abundances_sum > 1) {
    log_err(sln,
      "%s: molecule %d: the sum of the isotope abundances %g is invalid. "
      "It must be less than or equal to 1.\n",
      caller, molecule->id, abundances_sum);
    return RES_BAD_ARG;
  }

  if(molecule->concentration < 0) {
    log_err(sln, "%s: molecule %d: invalid concentration %g.\n",
      caller, molecule->id, molecule->concentration);
    return RES_BAD_ARG;
  }

  if(molecule->cutoff <= 0) {
    log_err(sln, "%s: molecule %d: invalid line cutoff %g.\n",
      caller, molecule->id, molecule->concentration);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
check_sln_mixture_create_args
  (struct sln_device* sln,
   const char* caller,
   const struct sln_mixture_create_args* args)
{
  double concentrations_sum = 0;
  size_t i;
  ASSERT(sln && caller);
  if(!args) return RES_BAD_ARG;

  /* Check lines data base */
  if(!args->metadata || !args->lines) {
    return RES_BAD_ARG;
  }

  /* Check the list of molecules to be taken into account */
  if(args->nmolecules >= SLN_MAX_MOLECULES_COUNT) {
    log_err(sln,
      "%s: too many molecules %lu regarding the maximum number of molecules "
      "(SLN_MAX_MOLECULES_COUNT = %lu).\n",
      caller,
      (unsigned long)args->nmolecules,
      (unsigned long)SLN_MAX_MOLECULES_COUNT);
    return RES_BAD_ARG;
  }

  concentrations_sum = 0;
  FOR_EACH(i, 0, args->nmolecules) {
    const res_T res = check_sln_molecule
      (sln, caller, args->metadata, args->molecules+i);
    if(res != RES_OK) return res;
    concentrations_sum += args->molecules[i].concentration;
  }

  if(concentrations_sum > 1) {
    log_err(sln,
      "%s: the sum of the molecule concentrations %g is invalid. "
      "It must be less than or equal to 1.\n",
      caller, concentrations_sum);
    return RES_BAD_ARG;
  }

  if(args->pressure < 0) {
    log_err(sln, "%s: invalid pressure %g.\n", caller, args->pressure);
    return RES_BAD_ARG;
  }

  if(args->temperature < 0) {
    log_err(sln, "%s: invalid temperature %g.\n", caller, args->temperature);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
create_mixture
  (struct sln_device* sln,
   const char* caller,
   struct sln_mixture** out_mixture)
{
  struct sln_mixture* mixture = NULL;
  res_T res = RES_OK;
  ASSERT(sln && caller && out_mixture);

  mixture = MEM_CALLOC(sln->allocator, 1, sizeof(struct sln_mixture));
  if(!mixture) {
    log_err(sln, "%s: could not allocate the mixture data structure.\n",
      caller);
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&mixture->ref);
  SLN(device_ref_get(sln));
  mixture->sln = sln;
  darray_line_init(sln->allocator, &mixture->lines);

exit:
  *out_mixture = mixture;
  return res;
error:
  if(mixture) { SLN(mixture_ref_put(mixture)); mixture = NULL; }
  goto exit;
}

static res_T
create_line_view
  (struct sln_device* sln,
   const struct sln_mixture_create_args* mixture_args,
   struct shtr_line_view** out_view)
{
  struct shtr_line_view_create_args view_args = SHTR_LINE_VIEW_CREATE_ARGS_NULL;
  struct shtr_line_view* view = NULL;
  size_t imol, iiso;
  res_T res = RES_OK;
  (void)sln;

  ASSERT(sln && mixture_args && out_view);

  view_args.wavenumber_range[0] = mixture_args->wavenumber_range[0];
  view_args.wavenumber_range[1] = mixture_args->wavenumber_range[1];
  view_args.nmolecules = mixture_args->nmolecules;
  view_args.pressure = mixture_args->pressure;

  FOR_EACH(imol, 0, mixture_args->nmolecules) {
    struct shtr_isotope_selection* view_mol = view_args.molecules + imol;
    const struct sln_molecule* mixture_mol = mixture_args->molecules + imol;

    view_mol->id = mixture_mol->id;
    view_mol->nisotopes = mixture_mol->nisotopes;
    view_mol->cutoff = mixture_mol->cutoff;
    FOR_EACH(iiso, 0, mixture_mol->nisotopes) {
      view_mol->isotope_ids_local[iiso] = mixture_mol->isotopes[iiso].id_local;
    }
  }

  res = shtr_line_view_create(mixture_args->lines, &view_args, &view);
  if(res != RES_OK) goto error;

exit:
  *out_view = view;
  return res;
error:
  if(view) { SHTR(line_view_ref_put(view)); view = NULL; }
  goto exit;
}

static void
molecules_params_reset(struct molecule_params params[SLN_MAX_MOLECULES_COUNT])
{
  size_t imol;
  ASSERT(params);

  /* Clean up the params */
  FOR_EACH(imol, 0, SLN_MAX_MOLECULES_COUNT) {
    size_t iiso;
    params[imol].concentration = -1;
    params[imol].cutoff = -1;
    FOR_EACH(iiso, 0, SLN_MAX_ISOTOPES_COUNT) {
      params[imol].isotopes_abundance[iiso] = -1;
    }
  }
}

static res_T
setup_molecule_params
  (struct sln_device* sln,
   const struct sln_mixture_create_args* args,
   struct molecule_params params[SLN_MAX_MOLECULES_COUNT])
{
  size_t imol;
  ASSERT(sln && args && params);
  (void)sln;

  molecules_params_reset(params);

  FOR_EACH(imol, 0, args->nmolecules) {
    const struct sln_molecule* mol = &args->molecules[imol];
    size_t iiso;
    ASSERT(mol->id < SLN_MAX_MOLECULES_COUNT);

    params[mol->id].concentration = mol->concentration;
    params[mol->id].cutoff = mol->cutoff;
    FOR_EACH(iiso, 0, mol->nisotopes) {
      const struct sln_isotope* iso = &mol->isotopes[iiso];
      ASSERT(iso->id_local < SLN_MAX_ISOTOPES_COUNT);

      params[mol->id].isotopes_abundance[iso->id_local] = iso->abundance;
    }
  }

  return RES_OK;
}

static INLINE res_T
store_input_args
  (struct sln_device* sln,
   const struct sln_mixture_create_args* args,
   struct sln_mixture* mixture)
{
  ASSERT(sln && args && mixture);
  (void)sln;
  mixture->wavenumber_range[0] = args->wavenumber_range[0];
  mixture->wavenumber_range[1] = args->wavenumber_range[1];
  mixture->temperature = args->temperature;
  mixture->pressure = args->pressure;
  SHTR(isotope_metadata_ref_get(args->metadata));
  mixture->metadata = args->metadata;
  return RES_OK;
}

static res_T
setup_lines(struct sln_mixture* mixture, const char* caller)
{
  size_t iline, nlines;
  res_T res = RES_OK;
  ASSERT(mixture && caller);

  SHTR(line_view_get_size(mixture->line_view, &nlines));

  res = darray_line_resize(&mixture->lines, nlines);
  if(res != RES_OK) {
    log_err(mixture->sln,
      "%s: error allocating the list of precomputed line data.\n",
      caller);
    goto error;
  }

  FOR_EACH(iline, 0, nlines) {
    res = line_setup(mixture, iline);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  darray_line_clear(&mixture->lines);
  goto exit;
}

static void
release_mixture(ref_T* ref)
{
  struct sln_mixture* mixture = CONTAINER_OF(ref, struct sln_mixture, ref);
  struct sln_device* sln = NULL;
  ASSERT(ref);
  sln = mixture->sln;
  if(mixture->metadata) SHTR(isotope_metadata_ref_put(mixture->metadata));
  if(mixture->line_view) SHTR(line_view_ref_put(mixture->line_view));
  darray_line_release(&mixture->lines);
  MEM_RM(sln->allocator, mixture);
  SLN(device_ref_put(sln));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sln_mixture_create
  (struct sln_device* sln,
   const struct sln_mixture_create_args* args,
   struct sln_mixture** out_mixture)
{
  struct sln_mixture* mixture = NULL;
  res_T res = RES_OK;

  if(!sln || !out_mixture) { res = RES_BAD_ARG; goto error; }
  res = check_sln_mixture_create_args(sln, FUNC_NAME, args);
  if(res != RES_OK) goto error;
  res = create_mixture(sln, FUNC_NAME, &mixture);
  if(res != RES_OK) goto error;

  #define CALL(Func) { if(RES_OK != (res = Func)) goto error; } (void)0
  CALL(create_line_view(sln, args, &mixture->line_view));
  CALL(setup_molecule_params(sln, args, mixture->molecules_params));
  CALL(store_input_args(sln, args, mixture));
  CALL(setup_lines(mixture, FUNC_NAME));
  #undef CALL

exit:
  if(out_mixture) *out_mixture = mixture;
  return res;
error:
  if(mixture) { SLN(mixture_ref_put(mixture)); mixture = NULL; }
  goto exit;
}

res_T
sln_mixture_create_from_stream
  (struct sln_device* sln,
   struct shtr* shtr,
   FILE* stream,
   struct sln_mixture** out_mixture)
{
  struct sln_mixture* mixture = NULL;
  size_t nlines = 0;
  int version = 0;
  res_T res = RES_OK;

  if(!sln || !shtr || !stream || !out_mixture) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = create_mixture(sln, FUNC_NAME, &mixture);
  if(res != RES_OK) goto error;

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      log_err(sln, "%s: error reading mixture data -- %s.\n",                  \
        FUNC_NAME, res_to_cstr(res));                                          \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&version, 1);
  if(version != SLN_MIXTURE_VERSION) {
    log_err(sln,
      "%s: unexpected mixture version %d. "
      "Expecting a mixture in version %d.\n",
      FUNC_NAME, version, SLN_MIXTURE_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }
  READ(mixture->molecules_params, SLN_MAX_MOLECULES_COUNT);

  res = shtr_isotope_metadata_create_from_stream(shtr, stream, &mixture->metadata);
  if(res != RES_OK) goto error;
  res = shtr_line_view_create_from_stream(shtr, stream, &mixture->line_view);
  if(res != RES_OK) goto error;

  READ(&nlines, 1);
  res = darray_line_resize(&mixture->lines, nlines);
  if(res != RES_OK) {
    log_err(sln, "%s: error allocating mixture lines -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }
  READ(darray_line_data_get(&mixture->lines), nlines);
#ifndef NDEBUG
  SHTR(line_view_get_size(mixture->line_view, &nlines));
  ASSERT(darray_line_size_get(&mixture->lines) == nlines);
#endif

  READ(&mixture->temperature, 1);
  READ(&mixture->pressure, 1);
  READ(&mixture->wavenumber_range, 1);
  #undef READ

exit:
  if(out_mixture) *out_mixture = mixture;
  return res;
error:
  if(mixture) { SLN(mixture_ref_put(mixture)); mixture = NULL; }
  goto exit;
}

res_T
sln_mixture_ref_get(struct sln_mixture* mixture)
{
  if(!mixture) return RES_BAD_ARG;
  ref_get(&mixture->ref);
  return RES_OK;
}

res_T
sln_mixture_ref_put(struct sln_mixture* mixture)
{
  if(!mixture) return RES_BAD_ARG;
  ref_put(&mixture->ref, release_mixture);
  return RES_OK;
}

res_T
sln_mixture_get_desc
  (const struct sln_mixture* mixture,
   struct sln_mixture_desc* desc)
{
  if(!mixture || !desc) return RES_BAD_ARG;
  desc->wavenumber_range[0] = mixture->wavenumber_range[0];
  desc->wavenumber_range[1] = mixture->wavenumber_range[1];
  desc->temperature = mixture->temperature;
  desc->pressure = mixture->pressure;
  desc->nlines = darray_line_size_get(&mixture->lines);
  return RES_OK;
}

res_T
sln_mixture_write(const struct sln_mixture* mixture, FILE* stream)
{
  size_t nlines = 0;
  res_T res = RES_OK;

  if(!mixture || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      log_err(mixture->sln, "%s: error writing the mixture.\n", FUNC_NAME);    \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&SLN_MIXTURE_VERSION, 1);
  WRITE(mixture->molecules_params, SLN_MAX_MOLECULES_COUNT);

  res = shtr_isotope_metadata_write(mixture->metadata, stream);
  if(res != RES_OK) goto error;
  res = shtr_line_view_write(mixture->line_view, stream);
  if(res != RES_OK) goto error;

  nlines = darray_line_size_get(&mixture->lines);
  WRITE(&nlines, 1);
  WRITE(darray_line_cdata_get(&mixture->lines), nlines);
  WRITE(&mixture->temperature, 1);
  WRITE(&mixture->pressure, 1);
  WRITE(mixture->wavenumber_range, 2);
  #undef WRITE

exit:
  return res;
error:
  goto exit;
}

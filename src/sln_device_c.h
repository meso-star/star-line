/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SLN_DEVICE_C_H
#define SLN_DEVICE_C_H

#include <rsys/logger.h>
#include <rsys/ref_count.h>

struct mem_allocator;

struct sln_device {
  struct mem_allocator* allocator;
  struct logger* logger;
  struct logger logger__; /* Default logger */
  int verbose;
  ref_T ref;
};

#endif /* SLN_DEVICE_C_H */

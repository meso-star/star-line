/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SLN_POLYLINE_H
#define SLN_POLYLINE_H

#include <rsys/rsys.h>

/* Forward declaration */
struct sln_device;
struct sln_vertex;

/* In place simplification of a polyline. Given a curve composed of line
 * segments, compute a similar curve with fewer points. The range of this new
 * polyline is  returned in `vertices_range'. */
extern LOCAL_SYM res_T
polyline_decimate
  (struct sln_device* sln,
   struct sln_vertex* vertices,
   size_t vertices_range[2],
   const float err, /* Relative error */
   const enum sln_mesh_type mesh_type);

#endif /* SLN_POLYLINE_H */

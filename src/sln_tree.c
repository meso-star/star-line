/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sln.h"
#include "sln_device_c.h"
#include "sln_line.h"
#include "sln_log.h"
#include "sln_mixture_c.h"
#include "sln_tree_c.h"

#include <star/shtr.h>

#include <rsys/algorithm.h>
#include <rsys/cstr.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_sln_tree_create_args
  (struct sln_device* sln,
   const char* caller,
   const struct sln_tree_create_args* args)
{
  ASSERT(sln && caller);
  if(!args) return RES_BAD_ARG;

  /* Currently only 1 line per leaf is accepted */
  if(args->max_nlines_per_leaf != 1) {
    log_err(sln,
      "%s: invalid maximum number of lines per leaf %lu. "
      "Currently it must be set to 1.\n",
      caller, (unsigned long)args->max_nlines_per_leaf);
    return RES_BAD_ARG;
  }

  if(args->nvertices_hint == 0) {
    log_err(sln,
      "%s: invalid hint on the number of vertices around the line center %lu.\n",
      caller, (unsigned long)args->nvertices_hint);
    return RES_BAD_ARG;
  }

  if(args->mesh_decimation_err < 0) {
    log_err(sln, "%s: invalid decimation error %g.\n",
      caller, args->mesh_decimation_err);
    return RES_BAD_ARG;
  }

  if((unsigned)args->mesh_type >= SLN_MESH_TYPES_COUNT__) {
    log_err(sln, "%s: invalid mesh type %d.\n", caller, args->mesh_type);
    return RES_BAD_ARG;
  }

  if((unsigned)args->line_profile >= SLN_LINE_PROFILES_COUNT__) {
    log_err(sln, "%s: invalid line profile %d.\n", caller, args->line_profile);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
create_tree
  (struct sln_device* sln,
   const char* caller,
   struct sln_tree** out_tree)
{
  struct sln_tree* tree = NULL;
  res_T res = RES_OK;
  ASSERT(sln && caller && out_tree);

  tree = MEM_CALLOC(sln->allocator, 1, sizeof(struct sln_tree));
  if(!tree) {
    log_err(sln, "%s: could not allocate the tree data structure.\n",
      caller);
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&tree->ref);
  darray_node_init(sln->allocator, &tree->nodes);
  darray_vertex_init(sln->allocator, &tree->vertices);

exit:
  *out_tree = tree;
  return res;
error:
  if(tree) { SLN(tree_ref_put(tree)); tree = NULL; }
  goto exit;
}

static INLINE res_T
store_input_args
  (struct sln_tree* tree,
   const struct sln_tree_create_args* args)
{
  ASSERT(args && tree);
  tree->max_nlines_per_leaf = args->max_nlines_per_leaf;
  tree->mesh_decimation_err = args->mesh_decimation_err;
  tree->mesh_type = args->mesh_type;
  tree->line_profile = args->line_profile;
  return RES_OK;
}

static INLINE int
cmp_nu_vtx(const void* key, const void* item)
{
  const float nu = *((const float*)key);
  const struct sln_vertex* vtx = item;
  if(nu < vtx->wavenumber) return -1;
  if(nu > vtx->wavenumber) return +1;
  return 0;
}

static void
release_tree(ref_T* ref)
{
  struct sln_tree* tree = CONTAINER_OF(ref, struct sln_tree, ref);
  struct sln_mixture* mixture = NULL;
  ASSERT(ref);
  mixture = tree->mixture;
  darray_node_release(&tree->nodes);
  darray_vertex_release(&tree->vertices);
  MEM_RM(mixture->sln->allocator, tree);
  SLN(mixture_ref_put(mixture));
}

/*******************************************************************************
 * Exported symbols
 ******************************************************************************/
res_T
sln_tree_create
  (struct sln_mixture* mixture,
   const struct sln_tree_create_args* args,
   struct sln_tree** out_tree)
{
  struct sln_tree* tree = NULL;
  res_T res = RES_OK;

  if(!mixture || !out_tree) { res = RES_BAD_ARG; goto error; }
  res = check_sln_tree_create_args(mixture->sln, FUNC_NAME, args);
  if(res != RES_OK) goto error;

  res = create_tree(mixture->sln, FUNC_NAME, &tree);
  if(res != RES_OK) goto error;
  SLN(mixture_ref_get(mixture));
  tree->mixture = mixture;

  res = store_input_args(tree, args);
  if(res != RES_OK) goto error;
  res = tree_build(tree, args);
  if(res != RES_OK) goto error;

exit:
  if(out_tree) *out_tree = tree;
  return res;
error:
  if(tree) { SLN(tree_ref_put(tree)); tree = NULL; }
  goto exit;
}

res_T
sln_tree_create_from_stream
  (struct sln_device* sln,
   struct shtr* shtr,
   FILE* stream,
   struct sln_tree** out_tree)
{
  struct sln_tree* tree = NULL;
  size_t n = 0;
  int version = 0;
  res_T res = RES_OK;

  if(!sln || !shtr || !stream || !out_tree) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = create_tree(sln, FUNC_NAME, &tree);
  if(res != RES_OK) goto error;

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      log_err(sln, "%s: error reading tree data -- %s.\n",                     \
        FUNC_NAME, res_to_cstr(res));                                          \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&version, 1);
  if(version != SLN_TREE_VERSION) {
    log_err(sln,
      "%s: unexpected tree version %d. Expecting a tree in version %d.\n",
      FUNC_NAME, version, SLN_TREE_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }

  READ(&n, 1);
  res = darray_node_resize(&tree->nodes, n);
  if(res != RES_OK) {
    log_err(sln, "%s: error allocating the tree nodes -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }
  READ(darray_node_data_get(&tree->nodes), n);

  READ(&n, 1);
  res = darray_vertex_resize(&tree->vertices, n);
  if(res != RES_OK) {
    log_err(sln, "%s: error allocatiing the tree vertices -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }
  READ(darray_vertex_data_get(&tree->vertices), n);

  READ(&tree->max_nlines_per_leaf, 1);
  READ(&tree->mesh_decimation_err, 1);
  READ(&tree->line_profile, 1);
  #undef READ

  res = sln_mixture_create_from_stream(sln, shtr, stream, &tree->mixture);
  if(res != RES_OK) goto error;

exit:
  if(out_tree) *out_tree = tree;
  return res;
error:
  if(tree) { SLN(tree_ref_put(tree)); tree = NULL; }
  goto exit;
}

res_T
sln_tree_ref_get(struct sln_tree* tree)
{
  if(!tree) return RES_BAD_ARG;
  ref_get(&tree->ref);
  return RES_OK;
}

res_T
sln_tree_ref_put(struct sln_tree* tree)
{
  if(!tree) return RES_BAD_ARG;
  ref_put(&tree->ref, release_tree);
  return RES_OK;
}

res_T
sln_tree_get_desc(const struct sln_tree* tree, struct sln_tree_desc* desc)
{
  if(!tree || !desc) return RES_BAD_ARG;
  desc->max_nlines_per_leaf = tree->max_nlines_per_leaf;
  desc->mesh_decimation_err = tree->mesh_decimation_err;
  desc->mesh_type = tree->mesh_type;
  desc->line_profile = tree->line_profile;
  return RES_OK;
}

const struct sln_node*
sln_tree_get_root(const struct sln_tree* tree)
{
  ASSERT(tree);
  if(darray_node_size_get(&tree->nodes)) {
    return darray_node_cdata_get(&tree->nodes);
  } else {
    return NULL;
  }
}

int
sln_node_is_leaf(const struct sln_node* node)
{
  ASSERT(node);
  return node->offset == 0;
}

const struct sln_node*
sln_node_get_child(const struct sln_node* node, const unsigned ichild)
{
  ASSERT(node && ichild <= 1);
  return node + node->offset + ichild;
}

size_t
sln_node_get_lines_count(const struct sln_node* node)
{
  ASSERT(node);
  return node->range[1] - node->range[0] + 1/*Both boundaries are inclusives*/;
}

res_T
sln_node_get_line
  (const struct sln_tree* tree,
   const struct sln_node* node,
   const size_t iline,
   const struct shtr_line** line)
{
  if(!tree || !node || iline > sln_node_get_lines_count(node))
    return RES_BAD_ARG;

  return shtr_line_view_get_line
    (tree->mixture->line_view, node->range[0] + iline, line);
}

res_T
sln_node_get_mesh
  (const struct sln_tree* tree,
   const struct sln_node* node,
   struct sln_mesh* mesh)
{
  if(!tree || !node || !mesh) return RES_BAD_ARG;
  mesh->vertices = darray_vertex_cdata_get(&tree->vertices) + node->ivertex;
  mesh->nvertices = node->nvertices;
  return RES_OK;
}

double
sln_node_eval
  (const struct sln_tree* tree,
   const struct sln_node* node,
   const double nu)
{
  double ka = 0;
  size_t iline;
  ASSERT(tree && node && nu >= 0);
  FOR_EACH(iline, node->range[0], node->range[1]+1) {
    ka += line_value(tree->mixture, iline, tree->line_profile, nu);
  }
  return ka;
}

double
sln_mesh_eval(const struct sln_mesh* mesh, const double wavenumber)
{
  const struct sln_vertex* vtx0 = NULL;
  const struct sln_vertex* vtx1 = NULL;
  const float nu = (float)wavenumber;
  size_t n; /* #vertices */
  float u; /* Linear interpolation paraeter */
  ASSERT(mesh && nu >= 0 && mesh->nvertices);

  n = mesh->nvertices;

  /* Handle special cases */
  if(n == 1) return mesh->vertices[0].ka;
  if(nu <= mesh->vertices[0].wavenumber) return mesh->vertices[0].ka;
  if(nu >= mesh->vertices[n-1].wavenumber) return mesh->vertices[n-1].ka;

  /* Dichotomic search of the mesh vertex whose wavenumber is greater than or
   * equal to the submitted wavenumber 'nu' */
  vtx1 = search_lower_bound(&nu, mesh->vertices, n, sizeof(*vtx1), cmp_nu_vtx);
  vtx0 = vtx1 - 1;
  ASSERT(vtx1); /* A vertex is necessary found ...*/
  ASSERT(vtx1 > mesh->vertices); /* ... and it cannot be the first one */
  ASSERT(vtx0->wavenumber < nu && nu <= vtx1->wavenumber);

  /* Compute the linear interpolation parameter */
  u = (nu - vtx0->wavenumber) / (vtx1->wavenumber - vtx0->wavenumber);
  u = CLAMP(u, 0, 1); /* Handle numerical imprecisions */

  if(u == 0) return vtx0->ka;
  if(u == 1) return vtx1->ka;
  return u*(vtx1->ka - vtx0->ka) + vtx0->ka;
}

res_T
sln_tree_write(const struct sln_tree* tree, FILE* stream)
{
  size_t n;
  res_T res = RES_OK;

  if(!tree || !stream) { res = RES_BAD_ARG; goto error; }

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      log_err(tree->mixture->sln, "%s: error writing the tree.\n", FUNC_NAME); \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&SLN_TREE_VERSION, 1);

  n = darray_node_size_get(&tree->nodes);
  WRITE(&n, 1);
  WRITE(darray_node_cdata_get(&tree->nodes), n);

  n = darray_vertex_size_get(&tree->vertices);
  WRITE(&n, 1);
  WRITE(darray_vertex_cdata_get(&tree->vertices), n);

  WRITE(&tree->max_nlines_per_leaf, 1);
  WRITE(&tree->mesh_decimation_err, 1);
  WRITE(&tree->line_profile, 1);
  #undef WRITE

  res = sln_mixture_write(tree->mixture, stream);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

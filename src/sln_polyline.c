/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sln.h"
#include "sln_device_c.h"
#include "sln_polyline.h"

#include <rsys/dynamic_array_size_t.h>
#include <rsys/float2.h>
#include <rsys/math.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
/* Given a set of points in [range[0], range[1]], find the point in
 * [range[0]+1, range[0]-1] whose maximize the error regarding its linear
 * approximation by the line (range[0], range[1]). Let K the real value of the point
 * and Kl its linear approximation, the error is computed as:
 *
 *    err = |K-Kl|/K */
static void
find_falsest_vertex
  (const struct sln_vertex* vertices,
   const size_t range[2],
   const enum sln_mesh_type mesh_type,
   size_t* out_ivertex, /* Index of the falsest vertex */
   float* out_err) /* Error of the falsest vertex */
{
  float p0[2], p1[2]; /* 1st and last point of the submitted range */
  float N[2], C; /* edge equation N.p + C  = 0 */
  float len;
  size_t ivertex;
  size_t imax; /* Index toward the falsest vertex */
  float err_max;
  int has_vertices_above = 0;
  ASSERT(vertices && range && range[0] < range[1]-1 && out_ivertex && out_err);
  ASSERT((unsigned)mesh_type < SLN_MESH_TYPES_COUNT__);
  (void)len;

  #define FETCH_VERTEX(Dst, Id) {                                              \
    (Dst)[0] = vertices[(Id)].wavenumber;                                      \
    (Dst)[1] = vertices[(Id)].ka;                                              \
  } (void)0
  FETCH_VERTEX(p0, range[0]);
  FETCH_VERTEX(p1, range[1]);

  /* Compute the normal of the edge [p0, p1]
   * N[0] = (p1 - p0).y
   * N[1] =-(p1 - p0).x */
  N[0] = p1[1] - p0[1];
  N[1] = p0[0] - p1[0];
  len = f2_normalize(N, N);
  ASSERT(len > 0);

  /* Compute the last parameter of the edge equation */
  C = -f2_dot(N, p0);

  imax = range[0]+1;
  err_max = 0;
  FOR_EACH(ivertex, range[0]+1, range[1]) {
    float p[2];
    float val;
    float err;

    FETCH_VERTEX(p, ivertex);

    if(N[0]*p[0] + N[1]*p[1] + C < 0) { /* The vertex is above the edge */
      has_vertices_above = 1;
    }

    /* Compute the linear approximation of p */
    val = -(N[0]*p[0] + C)/N[1];

    /* Compute the relative error of the linear approximation of p */
    err = absf(val - p[1]) / p[1];
    if(err > err_max) {
      imax = ivertex;
      err_max = err;
    }
  }
  #undef FETCH_VERTEX

  *out_ivertex = imax;
  /* To ensure an upper mesh, we cannot delete a vertex above the candidate
   * edge used to simplify the mesh. We therefore compel ourselves not to
   * simplify the polyline when such vertices are detected by returning an
   * infinite error */
  if(mesh_type == SLN_MESH_UPPER && has_vertices_above) {
    *out_err = (float)INF;
  } else {
    *out_err = err_max;
  }
}

static INLINE void
check_polyline_vertices
  (const struct sln_vertex* vertices,
   const size_t vertices_range[2])
{
#ifdef NDEBUG
  (void)vertices, (void)vertices_range;
#else
  size_t i;
  ASSERT(vertices);
  FOR_EACH(i, vertices_range[0]+1, vertices_range[1]+1) {
    CHK(vertices[i].wavenumber >= vertices[i-1].wavenumber);
  }
#endif
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
/* In place simplification of a polyline. Given a curve composed of line
 * segments, compute a similar curve with fewer points. In the following we
 * implement the algorithm described in:
 *
 * "Algorithms for the reduction of the number of points required to
 * represent a digitized line or its caricature" - David H. Douglas and Thomas
 * K Peucker, Cartographica: the international journal for geographic
 * information and geovisualization - 1973 */
res_T
polyline_decimate
  (struct sln_device* sln,
   struct sln_vertex* vertices,
   size_t vertices_range[2],
   const float err, /* Max relative error */
   const enum sln_mesh_type mesh_type)
{
  struct darray_size_t stack;
  size_t range[2] = {0, 0};
  size_t ivtx = 0;
  size_t nvertices = 0;
  res_T res = RES_OK;
  ASSERT(vertices && vertices_range && err >= 0);
  ASSERT(vertices_range[0] < vertices_range[1]);
  check_polyline_vertices(vertices, vertices_range);

  darray_size_t_init(sln->allocator, &stack);

  nvertices = vertices_range[1] - vertices_range[0] + 1;
  if(nvertices <= 2 || err == 0) goto exit; /* Nothing to simplify */

  /* Helper macros */
  #define PUSH(Stack, Val) {                                                   \
    res = darray_size_t_push_back(&(Stack), &(Val));                           \
    if(res != RES_OK) goto error;                                              \
  } (void)0
  #define POP(Stack, Val) {                                                    \
    const size_t sz = darray_size_t_size_get(&(Stack));                        \
    ASSERT(sz);                                                                \
    (Val) = darray_size_t_cdata_get(&(Stack))[sz-1];                           \
    darray_size_t_resize(&(Stack), sz-1);                                      \
  } (void)0

  range[0] = vertices_range[0];
  range[1] = vertices_range[1];
  ivtx = vertices_range[0] + 1;

  /* Push a dummy entry to allow "stack pop" once range[0] reaches range[1] */
  PUSH(stack, range[1]);

  while(range[0] != vertices_range[1]) {
    size_t imax;
    float err_max = -1;

    if(range[1] - range[0] > 1) {
      find_falsest_vertex(vertices, range, mesh_type, &imax, &err_max);
    }

    if(err_max > err) {
      /* Try to simplify a smaller polyline interval in [range[0], imax] */
      PUSH(stack, range[1]);
      range[1] = imax;
    } else {
      /* Remove all vertices in [range[0]+1, range[1]-1]] */
      vertices[ivtx++] = vertices[range[1]];

      /* Setup the next range */
      range[0] = range[1];
      POP(stack, range[1]);
    }
  }
  #undef PUSH
  #undef POP

  vertices_range[1] = ivtx - 1;

  check_polyline_vertices(vertices, vertices_range);

exit:
  darray_size_t_release(&stack);
  return res;
error:
  goto exit;
}

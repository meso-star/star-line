/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SLN_LINE_H
#define SLN_LINE_H

#include "sln.h"

#include <star/shtr.h>
#include <rsys/rsys.h>
#include <math.h>

struct line {
  double wavenumber; /* Line center wrt pressure in cm^-1 */
  double profile_factor; /* m^-1.cm^-1 (1e2*density*intensity) */
  double gamma_d; /* Doppler half width */
  double gamma_l; /* Lorentz half width */
};

/* Forward declaration */
struct sln_mixture;
struct sln_tree;
struct darray_vertex;

static INLINE double
line_center
  (const struct shtr_line* line,
   const double pressure) /* In atm */
{
  ASSERT(line && pressure >= 0);
  return line->wavenumber + line->delta_air * pressure;
}

extern LOCAL_SYM res_T
line_setup
  (struct sln_mixture* mixture,
   const size_t iline);

extern LOCAL_SYM double
line_value
  (const struct sln_mixture* mixture,
   const size_t iline,
   const enum sln_line_profile profile,
   const double wavenumber);

extern LOCAL_SYM res_T
line_mesh
  (struct sln_tree* tree,
   const size_t iline,
   const size_t nvertices_hint,
   /* Range of generated line vertices. Upper bound is inclusive */
   size_t vertices_range[2]);

#endif /* SLN_LINE_H */

/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafterf support */

#include "sln_device_c.h"
#include "sln_line.h"
#include "sln_log.h"
#include "sln_mixture_c.h"
#include "sln_tree_c.h"

#include <lblu.h>
#include <star/shtr.h>

#include <rsys/algorithm.h>
#include <rsys/cstr.h>
#include <rsys/dynamic_array_double.h>
#include <rsys/math.h>

#include <math.h> /* nextafterf */

#define T_REF 296.0 /* K */
#define AVOGADRO_NUMBER 6.02214076e23 /* molec.mol^-1 */
#define PERFECT_GAZ_CONSTANT 8.2057e-5 /* m^3.atm.mol^-1.K^-1 */

#define MIN_NVERTICES_HINT 8
#define MAX_NVERTICES_HINT 128
STATIC_ASSERT(IS_POW2(MIN_NVERTICES_HINT), MIN_NVERTICES_HINT_is_not_a_pow2);
STATIC_ASSERT(IS_POW2(MIN_NVERTICES_HINT), MAX_NVERTICES_HINT_is_not_a_pow2);

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static INLINE double
line_intensity
  (const double intensity_ref, /* Reference intensity [cm^-1/(molec.cm^2)] */
   const double lower_state_energy, /* [cm^-1] */
   const double partition_function,
   const double temperature, /* [K] */
   const double temperature_ref, /* [K] */
   const double wavenumber) /* [cm^-1] */
{
  const double C2 = 1.4388; /* 2nd Planck constant [K.cm] */

  const double fol = /* TODO ask to Yaniss why this variable is named fol */
    (1-exp(-C2*wavenumber/temperature))
  / (1-exp(-C2*wavenumber/temperature_ref));

  const double tmp =
    exp(-C2*lower_state_energy/temperature)
  / exp(-C2*lower_state_energy/temperature_ref);

  return intensity_ref * partition_function * tmp * fol ;
}

static res_T
line_profile_factor
  (const struct sln_mixture* mixture,
   const struct shtr_line* shtr_line,
   double* out_profile_factor)
{
  /* Star-HITRAN data */
  struct shtr_molecule molecule = SHTR_MOLECULE_NULL;
  const struct shtr_isotope* isotope = NULL;

  /* Mixture parameters */
  const struct molecule_params* mol_params = NULL;

  /* Miscellaneous */
  double iso_abundance;
  double density; /* In molec.cm^-3 */
  double intensity, intensity_ref; /* In cm^-1/(molec.cm^2) */
  double Q, Q_T, Q_Tref; /* Partition function */
  double nu_c; /* In cm^-1 */
  double profile_factor; /* In m^-1.cm^-1 */
  double gj; /* State independant degeneracy factor */
  double Ps; /* In atm */
  double T; /* Temperature */
  int molid; /* Molecule id */
  int isoid; /* Isotope id local to its molecule */

  res_T res = RES_OK;
  ASSERT(mixture && shtr_line && out_profile_factor);

  /* Fetch the molecule data */
  mol_params = mixture_get_molecule_params(mixture, shtr_line->molecule_id);
  SHTR(isotope_metadata_find_molecule
    (mixture->metadata, shtr_line->molecule_id, &molecule));
  ASSERT(!SHTR_MOLECULE_IS_NULL(&molecule));
  ASSERT(molecule.nisotopes > (size_t)shtr_line->isotope_id_local);
  isotope = molecule.isotopes + shtr_line->isotope_id_local;

  nu_c = line_center(shtr_line, mixture->pressure);

  /* Compute the intensity */
  Ps = mixture->pressure * mol_params->concentration;
  density = (AVOGADRO_NUMBER * Ps) / (PERFECT_GAZ_CONSTANT * mixture->temperature);
  density = density * 1e-6; /* Convert in molec.cm^-3 */

  /* Compute the partition function. TODO precompute it for molid/isoid */
  Q_Tref = isotope->Q296K;
  molid = shtr_line->molecule_id;
  isoid = shtr_line->isotope_id_local+1/*Local indices start at 1 in BD_TIPS*/;
  T = mixture->temperature;
  BD_TIPS_2017(&molid, &T, &isoid, &gj, &Q_T);
  if(Q_T <= 0) {
    log_err(mixture->sln,
      "molecule %d: isotope %d: invalid partition function at T=%g\n",
      molid, isoid, T);
    res = RES_BAD_ARG;
    goto error;
  }

  Q = Q_Tref/Q_T;

  /* Compute the intensity */
  iso_abundance = mol_params->isotopes_abundance[shtr_line->isotope_id_local];
  if(iso_abundance < 0) { /* Use default abundance */
    intensity_ref = shtr_line->intensity;
  } else {
    intensity_ref = shtr_line->intensity/isotope->abundance*iso_abundance;
  }
  intensity = line_intensity(intensity_ref, shtr_line->lower_state_energy, Q,
    mixture->temperature, T_REF, nu_c);

  profile_factor = 1.e2 * density * intensity; /* In m^-1.cm^-1 */

exit:
  *out_profile_factor = profile_factor;
  return res;
error:
  profile_factor = NaN;
  goto exit;
}

/* Regularly mesh the interval [wavenumber, wavenumber+spectral_length[. Note
 * that the upper bound is excluded, this means that the last vertex of the
 * interval is not emitted */
static INLINE res_T
regular_mesh
  (const double wavenumber, /* Wavenumber where the mesh begins [cm^-1] */
   const double spectral_length, /* Size of the spectral interval to mesh [cm^-1] */
   const size_t nvertices, /* #vertices to issue */
   struct darray_double* wavenumbers) /* List of issued vertices */
{
  /* Do not issue the vertex on the upper bound of the spectral range. That's
   * why we assume that the number of steps is equal to the number of vertices
   * and not to the number of vertices minus 1 */
  const double step = spectral_length / (double)nvertices;
  size_t ivtx;
  res_T res = RES_OK;
  ASSERT(wavenumber >= 0  && spectral_length > 0 && wavenumbers);

  FOR_EACH(ivtx, 0, nvertices) {
    const double nu = wavenumber + (double)ivtx*step;
    res = darray_double_push_back(wavenumbers, &nu);
    if(res != RES_OK) goto error;
  }
exit:
  return res;
error:
  goto exit;
}

/* The line is regularly discretized into a set of fragments of fixed size.
 * Their discretization is finer for the fragments around the center of the
 * line and becomes coarser as the fragments move away from it. Note that a
 * line is symmetrical in its center. As a consequence, the returned list is
 * only the set of wavenumbers from the line center to its upper bound. */
static res_T
regular_mesh_fragmented
  (const struct sln_mixture* mixture,
   const struct line* line,
   const struct molecule_params* mol_params,
   const double fragment_length,
   const size_t nvertices,
   struct darray_double* wavenumbers) /* List of issued vertices */
{
  /* Fragment parameters */
  double fragment_nu_min = 0; /* Lower bound of the fragment */
  size_t fragment_nvtx = 0; /* #vertices into the fragment */

  /* Miscellaneous */
  double line_nu_min = 0; /* In cm^-1 */
  double line_nu_max = 0; /* In cm^-1 */
  res_T res = RES_OK;

  ASSERT(mixture && line && mol_params && wavenumbers);
  ASSERT(fragment_length > 0);
  ASSERT(IS_POW2(nvertices));
  ASSERT(line->wavenumber + mol_params->cutoff > mixture->wavenumber_range[0]);
  ASSERT(line->wavenumber - mol_params->cutoff < mixture->wavenumber_range[1]);

  /* Compute the spectral range of the line from its center to its cutoff */
  line_nu_min = line->wavenumber;
  line_nu_max = line->wavenumber + mol_params->cutoff;

  /* Define the number of vertices for the first interval in [nu, gamma_l] */
  fragment_nu_min = line_nu_min;
  fragment_nvtx = MMAX(nvertices/2, 2);

  while(fragment_nu_min < line_nu_max) {
    const double spectral_length =
      MMIN(fragment_length, line_nu_max - fragment_nu_min);

    res = regular_mesh
      (fragment_nu_min, spectral_length, fragment_nvtx, wavenumbers);
    if(res != RES_OK) goto error;

    fragment_nu_min += fragment_length;
    fragment_nvtx = MMAX(fragment_nvtx/2, 2);
  }

  /* Register the last vertex, i.e. the upper bound of the spectral range */
  res = darray_double_push_back(wavenumbers, &line_nu_max);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  log_err(mixture->sln, "Error meshing the line -- %s.\n", res_to_cstr(res));
  goto exit;
}

static res_T
eval_mesh
  (const struct sln_mixture* mixture,
   const size_t iline,
   const struct darray_double* wavenumbers,
   const enum sln_line_profile line_profile,
   struct darray_double* values)
{
  const double* nu = NULL;
  double* ha = NULL;
  const struct shtr_line* shtr_line = NULL;
  size_t ivertex, nvertices;
  double range[2];
  double cutoff;
  res_T res = RES_OK;
  ASSERT(mixture && wavenumbers && values);

  nvertices = darray_double_size_get(wavenumbers);
  ASSERT(nvertices);

  res = darray_double_resize(values, nvertices);
  if(res != RES_OK) goto error;

  SHTR(line_view_get_line(mixture->line_view, iline, &shtr_line));

  /* Calculate the spectral range in which the vertices should be evaluated.
   * Recall that a line being symmetrical in its center, we will only evaluate
   * its upper half. Anyway, note that a line clipped by the spectral range is
   * no longer symmetric. In the following, we thus adjust the spectral range
   * to emit all the vertices needed to mesh the upper half _and_ the lower
   * half of a possibly clipped line */
  cutoff = MMAX
    (shtr_line->wavenumber - mixture->wavenumber_range[0],
     mixture->wavenumber_range[1] - shtr_line->wavenumber);
  range[0] = MMIN(shtr_line->wavenumber, mixture->wavenumber_range[0]);
  range[1] = shtr_line->wavenumber + cutoff;

  nu = darray_double_cdata_get(wavenumbers);
  ha = darray_double_data_get(values);
  FOR_EACH(ivertex, 0, nvertices) {
    const double nu_curr = nu[ivertex];
    const double nu_next = ivertex < nvertices - 1 ? nu[ivertex+1] : nu_curr;
    const double nu_prev = ivertex > 0 ? nu[ivertex-1] : nu_curr;

    /* Check if the vertex needs to be cut. Note that we do not evaluate a vertex
     * if it is out of spectral range and does not directly precede or succeed
     * a vertex that is not clipped. This ensures that the spectral boundaries
     * are always surrounded by vertices whose value has been evaluate. */
    if(nu_next < range[0] || range[1] < nu_prev) {
      ha[ivertex] = -1; /* Clip */
    } else {
      ha[ivertex] = line_value(mixture, iline, line_profile, nu_curr);
    }
  }

exit:
  return res;
error:
  log_err(mixture->sln, "Error evaluating the line mesh -- %s.\n",
    res_to_cstr(res));
  goto exit;
}

static void
snap_mesh_to_upper_bound
  (const struct darray_double* wavenumbers,
   struct darray_double* values)
{
  double* ha = NULL;
  size_t ivertex, nvertices;
  size_t ivertex_1st;
  ASSERT(wavenumbers && values);
  ASSERT(darray_double_size_get(wavenumbers) == darray_double_size_get(values));
  (void)wavenumbers;

  ha = darray_double_data_get(values);
  nvertices = darray_double_size_get(wavenumbers);

  /* Search for the first (non clipped) vertex */
  FOR_EACH(ivertex_1st, 0, nvertices) {
    if(ha[ivertex_1st] > 0) break;
  }
  ASSERT(ivertex_1st < nvertices);

  /* Ensure that the stored vertex value is an exclusive upper bound of the
   * original value. We do this by storing a value in single precision that is
   * strictly greater than its encoding in double precision */
  if(ha[ivertex_1st] != (float)ha[ivertex_1st]) {
    ha[ivertex_1st] = nextafterf((float)ha[ivertex_1st], FLT_MAX);
  }

  /* We have meshed the upper half of the line which is a strictly decreasing
   * function. To ensure that the mesh is an upper limit of this function,
   * simply align the value of each vertex with the value of the preceding
   * vertex */
  FOR_EACH_REVERSE(ivertex, nvertices-1, ivertex_1st) {
    if(ha[ivertex] < 0) continue; /* The vertex is clipped */
    ha[ivertex] = ha[ivertex-1];
  }
}

static INLINE int
cmp_dbl(const void* a, const void* b)
{
  const double key = *((const double*)a);
  const double item = *((const double*)b);
  if(key < item) return -1;
  if(key > item) return +1;
  return 0;
}

/* Return the value of the vertex whose wavenumber is greater than 'nu' */
static INLINE double
next_vertex_value
  (const double nu,
   const struct darray_double* wavenumbers,
   const struct darray_double* values)
{
  const double* wnum = NULL;
  size_t ivertex = 0;
  ASSERT(wavenumbers && values);

  wnum = search_lower_bound
    (&nu,
     darray_double_cdata_get(wavenumbers),
     darray_double_size_get(wavenumbers),
     sizeof(double),
     cmp_dbl);
  ASSERT(wnum); /* It necessary exists */

  ivertex = (size_t)(wnum - darray_double_cdata_get(wavenumbers));
  ASSERT(ivertex < darray_double_size_get(values));

  return darray_double_cdata_get(values)[ivertex];
}

/* Append the line mesh into the vertices array */
static res_T
save_line_mesh
  (struct sln_tree* tree,
   const size_t iline,
   const struct molecule_params* mol_params,
   const double spectral_range[2],
   const struct darray_double* wavenumbers,
   const struct darray_double* values,
   size_t vertices_range[2]) /* Range into which the line vertices are saved */
{
  const struct line* line = NULL;
  const double* wnums = NULL;
  const double* vals = NULL;
  size_t nvertices;
  size_t nwavenumbers;
  size_t line_nvertices;
  size_t ivertex;
  size_t i;
  res_T res = RES_OK;

  ASSERT(tree && mol_params && wavenumbers && values && vertices_range);
  ASSERT(darray_double_size_get(wavenumbers) == darray_double_size_get(values));
  ASSERT(spectral_range && spectral_range[0] <= spectral_range[1]);

  line = darray_line_cdata_get(&tree->mixture->lines) + iline;
  nvertices = darray_vertex_size_get(&tree->vertices);
  nwavenumbers = darray_double_size_get(wavenumbers);

  /* Compute the overall number of vertices of the line */
  line_nvertices = nwavenumbers
    * 2 /* The line is symmetrical in its center */
    - 1;/* Do not duplicate the line center */

  /* Allocate the list of line vertices */
  res = darray_vertex_resize(&tree->vertices, nvertices + line_nvertices);
  if(res != RES_OK) {
    log_err(tree->mixture->sln, "Error allocating the line vertices -- %s.\n",
      res_to_cstr(res));
    goto error;
  }

  wnums = darray_double_cdata_get(wavenumbers);
  vals = darray_double_cdata_get(values);
  i = nvertices;

  #define MIRROR(Nu) (2*line->wavenumber - (Nu))

  /* Emit the 1st vertex for the lower bound of the spectral range if the line
   * is clipped by it */
  if(line->wavenumber - mol_params->cutoff <= spectral_range[0]) {
    struct sln_vertex* vtx = darray_vertex_data_get(&tree->vertices)+i;
    const double nu = spectral_range[0];
    double ha = 0;

    switch(tree->mesh_type) {
      case SLN_MESH_UPPER:
        ha = nu > line->wavenumber
          ? next_vertex_value(nu, wavenumbers, values)
          : next_vertex_value(MIRROR(nu), wavenumbers, values);
        break;

      case SLN_MESH_USUAL:
        ha = line_value(tree->mixture, iline, tree->line_profile, nu);
        break;

      default: FATAL("Unreachable code.\n"); break;
    }
    vtx->wavenumber = (float)nu;
    vtx->ka = (float)ha;
    ++i;
  }

  /* Copy the vertices of the line for its lower half */
  FOR_EACH_REVERSE(ivertex, nwavenumbers-1, 0) {
    struct sln_vertex* vtx = darray_vertex_data_get(&tree->vertices)+i;
    const double nu = MIRROR(wnums[ivertex]);
    const double ha = vals[ivertex];

    /* The wavenumber is less than the lower bound of the spectral range.
     * Discard the vertex */
    if(nu <= spectral_range[0]) continue;

    /* The wavenumber is greater than the upper bound of the spectral range.
     * Stop the copy since the remaining vertices are greater than the current
     * one and are thus necessary out of the spectral range */
    if(nu >= spectral_range[1]) break;

    vtx->wavenumber = (float)nu;
    vtx->ka = (float)ha;
    ++i;
  }

  /* Copy the vertices of the line for its upper half */
  FOR_EACH(ivertex, 0, nwavenumbers) {
    struct sln_vertex* vtx = darray_vertex_data_get(&tree->vertices)+i;
    const double nu = wnums[ivertex];
    const double ha = vals[ivertex];

    /* The wavenumber is less than the lower bound of the spectral range.
     * Discard the vertex */
    if(nu <= spectral_range[0]) continue;

    /* The wavenumber is greater than the upper bound of the spectral range.
     * Stop the copy since the remaining vertices are greater than the current
     * one and are thus necessary out of the spectral range */
    if(nu >= spectral_range[1]) break;

    vtx->wavenumber = (float)nu;
    vtx->ka = (float)ha;
    ++i;
  }

  /* Emit the last vertex for the upper bound of the spectral range if the line
   * is clipped by it */
  if(line->wavenumber + mol_params->cutoff >= spectral_range[1]) {
    struct sln_vertex* vtx = darray_vertex_data_get(&tree->vertices)+i;
    const double nu = spectral_range[1];
    double ha = 0;

    switch(tree->mesh_type) {
      case SLN_MESH_UPPER:
        ha = nu > line->wavenumber
          ? next_vertex_value(nu, wavenumbers, values)
          : next_vertex_value(MIRROR(nu), wavenumbers, values);
        break;

      case SLN_MESH_USUAL:
        ha = line_value(tree->mixture, iline, tree->line_profile, nu);
        break;

      default: FATAL("Unreachable code.\n"); break;
    }
    vtx->wavenumber = (float)nu;
    vtx->ka = (float)ha;
    ++i;
  }

  #undef MIRROR

  /* Several vertices could be skipped due to the clipping of the line by the
   * spectral range. Resize the list of vertices to the effective number of
   * registered vertices. */
  CHK(darray_vertex_resize(&tree->vertices, i) == RES_OK);

  /* Setup the range of the line vertices */
  vertices_range[0] = nvertices;
  vertices_range[1] = i-1;

exit:
  return res;
error:
  darray_vertex_resize(&tree->vertices, nvertices);
  goto exit;
}


/*******************************************************************************
 * Local function
 ******************************************************************************/
res_T
line_setup(struct sln_mixture* mixture, const size_t iline)
{
  struct shtr_molecule molecule = SHTR_MOLECULE_NULL;
  const struct shtr_line* shtr_line = NULL;
  struct line* line = NULL;
  double molar_mass = 0; /* In kg.mol^-1 */
  const struct molecule_params* mol_params = NULL;
  res_T res = RES_OK;
  ASSERT(mixture && iline < darray_line_size_get(&mixture->lines));

  line = darray_line_data_get(&mixture->lines) + iline;

  SHTR(line_view_get_line(mixture->line_view, iline, &shtr_line));
  SHTR(isotope_metadata_find_molecule
    (mixture->metadata, shtr_line->molecule_id, &molecule));
  ASSERT(!SHTR_MOLECULE_IS_NULL(&molecule));
  ASSERT(molecule.nisotopes > (size_t)shtr_line->isotope_id_local);
  mol_params = mixture_get_molecule_params(mixture, shtr_line->molecule_id);

  /* Convert the molar mass of the line from g.mol^-1 to kg.mol^-1 */
  molar_mass = molecule.isotopes[shtr_line->isotope_id_local].molar_mass*1e-3;

  /* Setup the line */
  res = line_profile_factor(mixture, shtr_line, &line->profile_factor);
  if(res != RES_OK) goto error;

  line->wavenumber = line_center(shtr_line, mixture->pressure);
  line->gamma_d = sln_compute_line_half_width_doppler
    (line->wavenumber, molar_mass, mixture->temperature);
  line->gamma_l = sln_compute_line_half_width_lorentz(shtr_line->gamma_air,
    shtr_line->gamma_self, mixture->pressure, mol_params->concentration);

exit:
  return res;
error:
  goto exit;
}

double
line_value
  (const struct sln_mixture* mixture,
   const size_t iline,
   const enum sln_line_profile line_profile,
   const double wavenumber)
{
  const struct line* line = NULL;
  const struct shtr_line* shtr_line = NULL;
  const struct molecule_params* mol_params = NULL;
  double profile = 0;
  ASSERT(mixture && iline < darray_line_size_get(&mixture->lines));

  /* Retrieve the molecular parameters of the line to be mesh */
  SHTR(line_view_get_line(mixture->line_view, iline, &shtr_line));
  mol_params = mixture_get_molecule_params(mixture, shtr_line->molecule_id);

  line = darray_line_cdata_get(&mixture->lines) + iline;

  if(wavenumber < line->wavenumber - mol_params->cutoff
  || wavenumber > line->wavenumber + mol_params->cutoff) {
    return 0;
  }

  switch(line_profile) {
    case SLN_LINE_PROFILE_VOIGT:
      profile = sln_compute_voigt_profile
        (wavenumber, line->wavenumber, line->gamma_d, line->gamma_l);
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return line->profile_factor * profile;
}

res_T
line_mesh
  (struct sln_tree* tree,
   const size_t iline,
   const size_t nvertices_hint,
   size_t vertices_range[2])
{
  struct darray_double values; /* List of evaluated values */
  struct darray_double wavenumbers; /* List of considered wavenumbers */
  const struct shtr_line* shtr_line = NULL;
  const struct molecule_params* mol_params = NULL;
  const struct line* line = NULL;
  size_t nvertices_adjusted = 0; /* computed from nvertices_hint */
  res_T res = RES_OK;
  ASSERT(tree && nvertices_hint);
  ASSERT(iline < darray_line_size_get(&tree->mixture->lines));

  line = darray_line_cdata_get(&tree->mixture->lines) + iline;
  SHTR(line_view_get_line(tree->mixture->line_view, iline, &shtr_line));

  darray_double_init(tree->mixture->sln->allocator, &values);
  darray_double_init(tree->mixture->sln->allocator, &wavenumbers);

  /* Adjust the hint on the number of vertices. This is not actually the real
   * number of vertices but an adjusted hint on it. This new value ensures that
   * it is a power of 2 included in [MIN_NVERTICES_HINT, MAX_NVERTICES_HINT] */
  nvertices_adjusted = CLAMP
    (nvertices_hint, MIN_NVERTICES_HINT, MAX_NVERTICES_HINT);
  nvertices_adjusted = round_up_pow2(nvertices_adjusted);

  /* Retrieve the molecular parameters of the line to be mesh */
  mol_params = mixture_get_molecule_params(tree->mixture, shtr_line->molecule_id);

  /* Emit the vertex coordinates, i.e. the wavenumbers */
  res = regular_mesh_fragmented(tree->mixture, line, mol_params, line->gamma_l,
    nvertices_adjusted, &wavenumbers);
  if(res != RES_OK) goto error;

  /* Evaluate the mesh vertices, i.e. define the line value for the list of
   * wavenumbers */
  eval_mesh(tree->mixture, iline, &wavenumbers, tree->line_profile, &values);

  switch(tree->mesh_type) {
    case SLN_MESH_UPPER:
      snap_mesh_to_upper_bound(&wavenumbers, &values);
      break;
    case SLN_MESH_USUAL: /* Do nothing */ break;
    default: FATAL("Unreachable code.\n"); break;
  }

  res = save_line_mesh(tree, iline, mol_params, tree->mixture->wavenumber_range,
    &wavenumbers, &values, vertices_range);
  if(res != RES_OK) goto error;

exit:
  darray_double_release(&values);
  darray_double_release(&wavenumbers);
  return res;
error:
  goto exit;
}

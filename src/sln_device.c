/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sln.h"
#include "sln_device_c.h"
#include "sln_log.h"

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_sln_device_create_args(const struct sln_device_create_args* args)
{
  return args ? RES_OK : RES_BAD_ARG;
}

static void
release_sln_device(ref_T* ref)
{
  struct sln_device* sln = CONTAINER_OF(ref, struct sln_device, ref);
  ASSERT(ref);
  if(sln->logger == &sln->logger__) logger_release(&sln->logger__);
  MEM_RM(sln->allocator, sln);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sln_device_create
  (const struct sln_device_create_args* args,
   struct sln_device** out_sln)
{
  struct mem_allocator* allocator = NULL;
  struct sln_device* sln = NULL;
  res_T res = RES_OK;

  if(!out_sln) { res = RES_BAD_ARG; goto error; }
  res = check_sln_device_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  sln = MEM_CALLOC(allocator, 1, sizeof(*sln));
  if(!sln) {
    #define ERR_STR "Could not allocate the Star-Line data structure.\n"
    if(args->logger) {
      logger_print(args->logger, LOG_ERROR, ERR_STR);
    } else {
      fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
    }
    #undef ERR_STR
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&sln->ref);
  sln->allocator = allocator;
  sln->verbose = args->verbose;
  if(args->logger) {
    sln->logger = args->logger;
  } else {
    setup_log_default(sln);
  }

exit:
  if(out_sln) *out_sln = sln;
  return res;
error:
  if(sln) { SLN(device_ref_put(sln)); sln = NULL; }
  goto exit;
}

res_T
sln_device_ref_get(struct sln_device* sln)
{
  if(!sln) return RES_BAD_ARG;
  ref_get(&sln->ref);
  return RES_OK;
}

res_T
sln_device_ref_put(struct sln_device* sln)
{
  if(!sln) return RES_BAD_ARG;
  ref_put(&sln->ref, release_sln_device);
  return RES_OK;
}

/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "test_sln_lines.h"
#include "sln.h"

#include <star/shtr.h>

#include <rsys/algorithm.h>
#include <rsys/math.h>
#include <rsys/mem_allocator.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static int
cmp_line(const void* key, const void* item)
{
  const struct shtr_line* line0 = key;
  const struct shtr_line* line1 = item;
  CHK(key && item);
       if(line0->wavenumber < line1->wavenumber) return -1;
  else if(line0->wavenumber > line1->wavenumber) return +1;
  else return 0;
}

/* Return the index of the line in the line_list or SIZE_MAX if the line does
 * not exist */
static INLINE size_t
find_line
  (const struct shtr_line_list* line_list,
   const struct shtr_line* line)
{
  const struct shtr_line* list = NULL;
  const struct shtr_line* found = NULL;
  size_t iline, nlines;

  CHK(shtr_line_list_get(line_list, &list) == RES_OK);
  CHK(shtr_line_list_get_size(line_list, &nlines) == RES_OK);

  /* Dichotomous search wrt the wavenumber of lines */
  found = search_lower_bound
    (line, list, nlines, sizeof(struct shtr_line), cmp_line);
  if(!found) return SIZE_MAX;

  /* Find a line with the same wavenumber as the one searched for and whose
   * other member variables are also equal to those of the line searched for */
  CHK(found >= list);
  iline = (size_t)(found - list);
  while(list[iline].wavenumber == line->wavenumber
    && !shtr_line_eq(&list[iline], line)
    && iline < nlines) {
    ++iline;
  }

  if(shtr_line_eq(&list[iline], line)) {
    return iline;
  } else {
    return SIZE_MAX;
  }
}

/* This test assumes that all the lines contained into the list are
 * partitionned in the tree */
static void
check_tree
  (const struct sln_tree* tree,
   const struct shtr_line_list* line_list)
{
  #define STACK_SIZE 64
  const struct shtr_line* list = NULL;
  const struct sln_node* stack[STACK_SIZE] = {NULL};
  struct sln_tree_desc desc = SLN_TREE_DESC_NULL;
  size_t istack = 0;
  const struct sln_node* node = NULL;

  char* found_lines = NULL;
  size_t found_nlines = 0;
  size_t line_list_sz;

  CHK(shtr_line_list_get_size(line_list, &line_list_sz) == RES_OK);
  CHK(shtr_line_list_get(line_list, &list) == RES_OK);

  CHK(found_lines = mem_calloc(line_list_sz, sizeof(char)));

  CHK(sln_tree_get_desc(tree, &desc) == RES_OK);

  node = sln_tree_get_root(tree);
  while(node) {
    if(!sln_node_is_leaf(node)) {
      CHK(sln_node_get_lines_count(node) > desc.max_nlines_per_leaf);

      ASSERT(istack < STACK_SIZE);
      stack[istack++] = sln_node_get_child(node, 1); /* Push the child 1 */
      node = sln_node_get_child(node, 0); /* Handle the child 0 */

    } else {
      size_t iline, nlines;

      nlines = sln_node_get_lines_count(node);
      CHK(nlines <= desc.max_nlines_per_leaf);
      FOR_EACH(iline, 0, nlines) {
        const struct shtr_line* line = NULL;
        size_t found_iline = 0;
        CHK(sln_node_get_line(tree, node, iline, &line) == RES_OK);
        found_iline = find_line(line_list, line);
        CHK(found_iline != SIZE_MAX); /* Line is found */

        if(!found_lines[found_iline]) {
          found_nlines += 1;
          found_lines[found_iline] = 1;
        }
      }

      node = istack ? stack[--istack] : NULL; /* Pop the next node */
    }
  }

  /* Check that all lines are found */
  CHK(found_nlines == line_list_sz);

  mem_rm(found_lines);
  #undef STACK_SIZE
}

static void
dump_line(FILE* stream, const struct sln_mesh* mesh)
{
  size_t i;
  CHK(mesh);
  FOR_EACH(i, 0, mesh->nvertices) {
    fprintf(stream, "%g %g\n",
      mesh->vertices[i].wavenumber,
      mesh->vertices[i].ka);
  }
}

static void
test_tree(struct sln_mixture* mixture, const struct shtr_line_list* line_list)
{
  struct sln_tree_create_args tree_args = SLN_TREE_CREATE_ARGS_DEFAULT;
  struct sln_tree_desc desc = SLN_TREE_DESC_NULL;
  struct sln_mesh mesh = SLN_MESH_NULL;
  struct sln_tree* tree = NULL;
  const struct sln_node* node = NULL;
  const struct shtr_line* line = NULL;
  size_t nlines = 0;

  CHK(mixture && line_list);

  CHK(sln_tree_create(NULL, &tree_args, &tree) == RES_BAD_ARG);
  CHK(sln_tree_create(mixture, NULL, &tree) == RES_BAD_ARG);
  CHK(sln_tree_create(mixture, &tree_args, NULL) == RES_BAD_ARG);
  CHK(sln_tree_create(mixture, &tree_args, &tree) == RES_OK);

  CHK(shtr_line_list_get_size(line_list, &nlines) == RES_OK);

  CHK(sln_tree_get_desc(NULL, &desc) == RES_BAD_ARG);
  CHK(sln_tree_get_desc(tree, NULL) == RES_BAD_ARG);
  CHK(sln_tree_get_desc(tree, &desc) == RES_OK);

  CHK(desc.max_nlines_per_leaf == tree_args.max_nlines_per_leaf);
  CHK(desc.mesh_decimation_err == tree_args.mesh_decimation_err);
  CHK(desc.mesh_type == tree_args.mesh_type);
  CHK(desc.line_profile == tree_args.line_profile);

  CHK(node = sln_tree_get_root(tree));
  while(!sln_node_is_leaf(node)) {
    node = sln_node_get_child(node, 0);
  }
  CHK(sln_node_get_lines_count(node) <= tree_args.max_nlines_per_leaf);
  CHK(sln_node_get_line(NULL, node, 0, &line) == RES_BAD_ARG);
  CHK(sln_node_get_line(tree, NULL, 0, &line) == RES_BAD_ARG);
  CHK(sln_node_get_line(tree, node, tree_args.max_nlines_per_leaf+1, &line)
    == RES_BAD_ARG);
  CHK(sln_node_get_line(tree, node, 0, NULL) == RES_BAD_ARG);
  CHK(sln_node_get_line(tree, node, 0, &line) == RES_OK);
  CHK(sln_node_get_line(tree, sln_tree_get_root(tree), 0, &line) == RES_OK);

  CHK(sln_node_get_mesh(NULL, node, &mesh) == RES_BAD_ARG);
  CHK(sln_node_get_mesh(tree, NULL, &mesh) == RES_BAD_ARG);
  CHK(sln_node_get_mesh(tree, node, NULL) == RES_BAD_ARG);
  CHK(sln_node_get_mesh(tree, node, &mesh) == RES_OK);

  CHK(node = sln_tree_get_root(tree));
  CHK(sln_node_get_mesh(tree, node, &mesh) == RES_OK);

  dump_line(stdout, &mesh);
  check_tree(tree, line_list);

  CHK(sln_tree_ref_get(NULL) == RES_BAD_ARG);
  CHK(sln_tree_ref_get(tree) == RES_OK);
  CHK(sln_tree_ref_put(NULL) == RES_BAD_ARG);
  CHK(sln_tree_ref_put(tree) == RES_OK);
  CHK(sln_tree_ref_put(tree) == RES_OK);

  tree_args.max_nlines_per_leaf = 0;
  CHK(sln_tree_create(mixture, &tree_args, &tree) == RES_BAD_ARG);

  tree_args.max_nlines_per_leaf = 1;
  tree_args.mesh_decimation_err = -1;
  CHK(sln_tree_create(mixture, &tree_args, &tree) == RES_BAD_ARG);

  tree_args.mesh_decimation_err = 1e-1f;
  tree_args.mesh_type = SLN_MESH_TYPES_COUNT__;
  CHK(sln_tree_create(mixture, &tree_args, &tree) == RES_BAD_ARG);

  tree_args.mesh_type = SLN_MESH_USUAL;
  tree_args.line_profile = SLN_LINE_PROFILES_COUNT__;
  CHK(sln_tree_create(mixture, &tree_args, &tree) == RES_BAD_ARG);

  tree_args.line_profile = SLN_LINE_PROFILE_VOIGT;
  CHK(sln_tree_create(mixture, &tree_args, &tree) == RES_OK);
  CHK(sln_tree_ref_put(tree) == RES_OK);
}

static void
check_mesh_equality(const struct sln_mesh* mesh1, const struct sln_mesh* mesh2)
{
  size_t i;
  CHK(mesh1 && mesh2);
  CHK(mesh1->nvertices == mesh2->nvertices);

  FOR_EACH(i, 0, mesh1->nvertices) {
    CHK(mesh1->vertices[i].wavenumber == mesh2->vertices[i].wavenumber);
    CHK(mesh1->vertices[i].ka == mesh2->vertices[i].ka);
  }
}

static void
check_node_equality
  (const struct sln_tree* tree1,
   const struct sln_tree* tree2,
   const struct sln_node* node1,
   const struct sln_node* node2)
{
  struct sln_mesh mesh1 = SLN_MESH_NULL;
  struct sln_mesh mesh2 = SLN_MESH_NULL;
  size_t iline, nlines;

  CHK(node1 && node2);
  CHK(sln_node_is_leaf(node1) == sln_node_is_leaf(node2));
  CHK(sln_node_get_lines_count(node1) == sln_node_get_lines_count(node2));
  nlines = sln_node_get_lines_count(node1);

  FOR_EACH(iline, 0, nlines) {
    const struct shtr_line* line1 = NULL;
    const struct shtr_line* line2 = NULL;
    CHK(sln_node_get_line(tree1, node1, iline, &line1) == RES_OK);
    CHK(sln_node_get_line(tree2, node2, iline, &line2) == RES_OK);
    CHK(shtr_line_eq(line1, line2));
  }

  CHK(sln_node_get_mesh(tree1, node1, &mesh1) == RES_OK);
  CHK(sln_node_get_mesh(tree2, node2, &mesh2) == RES_OK);
  check_mesh_equality(&mesh1, &mesh2);
}

static void
check_tree_equality
  (const struct sln_tree* tree1,
   const struct sln_tree* tree2)
{
  const struct sln_node* stack[128] = {NULL};
  int istack = 0;

  struct sln_tree_desc desc1 = SLN_TREE_DESC_NULL;
  struct sln_tree_desc desc2 = SLN_TREE_DESC_NULL;
  const struct sln_node* node1 = NULL;
  const struct sln_node* node2 = NULL;

  CHK(sln_tree_get_desc(tree1, &desc1) == RES_OK);
  CHK(sln_tree_get_desc(tree2, &desc2) == RES_OK);
  CHK(desc1.max_nlines_per_leaf == desc2.max_nlines_per_leaf);
  CHK(desc1.mesh_decimation_err == desc2.mesh_decimation_err);
  CHK(desc1.line_profile == desc2.line_profile);

  stack[istack++] = NULL;
  stack[istack++] = NULL;

  node1 = sln_tree_get_root(tree1);
  node2 = sln_tree_get_root(tree2);

  while(node1 || node2) {
    CHK((!node1 && !node2) || (node1 && node2));
    check_node_equality(tree1, tree2, node1, node2);

    if(sln_node_is_leaf(node1)) {
      node2 = stack[--istack];
      node1 = stack[--istack];
    } else {
      stack[istack++] = sln_node_get_child(node1, 1);
      stack[istack++] = sln_node_get_child(node2, 1);
      node1 = sln_node_get_child(node1, 0);
      node2 = sln_node_get_child(node2, 0);
    }
  }
}

static void
test_tree_serialization
  (struct sln_device* sln,
   struct shtr* shtr,
   struct sln_mixture* mixture)
{
  struct sln_tree_create_args tree_args = SLN_TREE_CREATE_ARGS_DEFAULT;
  struct sln_tree* tree1 = NULL;
  struct sln_tree* tree2 = NULL;
  FILE* fp = NULL;

  CHK(sln_tree_create(mixture, &tree_args, &tree1) == RES_OK);

  CHK(fp = tmpfile());
  CHK(sln_tree_write(NULL, fp) == RES_BAD_ARG);
  CHK(sln_tree_write(tree1, NULL) == RES_BAD_ARG);
  CHK(sln_tree_write(tree1, fp) == RES_OK);
  rewind(fp);

  CHK(sln_tree_create_from_stream(NULL, shtr, fp, &tree2) == RES_BAD_ARG);
  CHK(sln_tree_create_from_stream(sln, NULL, fp, &tree2) == RES_BAD_ARG);
  CHK(sln_tree_create_from_stream(sln, shtr, NULL, &tree2) == RES_BAD_ARG);
  CHK(sln_tree_create_from_stream(sln, shtr, fp, NULL) == RES_BAD_ARG);
  CHK(sln_tree_create_from_stream(sln, shtr, fp, &tree2) == RES_OK);
  fclose(fp);

  check_tree_equality(tree1, tree2);

  CHK(sln_tree_ref_put(tree1) == RES_OK);
  CHK(sln_tree_ref_put(tree2) == RES_OK);
}

/*******************************************************************************
 * Test function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sln_device_create_args dev_args = SLN_DEVICE_CREATE_ARGS_DEFAULT;
  struct sln_mixture_create_args mixture_args = SLN_MIXTURE_CREATE_ARGS_DEFAULT;
  struct sln_device* sln = NULL;
  struct sln_mixture* mixture = NULL;

  struct shtr_create_args shtr_args = SHTR_CREATE_ARGS_DEFAULT;
  struct shtr* shtr = NULL;
  struct shtr_line_list* line_list = NULL;
  struct shtr_isotope_metadata* metadata = NULL;

  FILE* fp_lines = NULL;
  FILE* fp_mdata = NULL;
  (void)argc, (void)argv;

  /* Generate the file of the isotope metadata */
  CHK(fp_mdata = tmpfile());
  fprintf(fp_mdata, "Molecule # Iso Abundance Q(296K) gj Molar Mass(g)\n");
  write_shtr_molecule(fp_mdata, &g_H2O);
  write_shtr_molecule(fp_mdata, &g_CO2);
  write_shtr_molecule(fp_mdata, &g_O3);
  rewind(fp_mdata);

  /* Generate the file of lines */
  CHK(fp_lines = tmpfile());
  write_shtr_lines(fp_lines, g_lines, g_nlines);
  rewind(fp_lines);

  /* Load the isotope metadata and the lines */
  shtr_args.verbose = 1;
  CHK(shtr_create(&shtr_args, &shtr) == RES_OK);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp_mdata, NULL, &metadata) == RES_OK);
  CHK(shtr_line_list_load_stream(shtr, fp_lines, NULL, &line_list) == RES_OK);

  CHK(fclose(fp_lines) == 0);
  CHK(fclose(fp_mdata) == 0);

  dev_args.verbose = 1;
  CHK(sln_device_create(&dev_args, &sln) == RES_OK);

  /* Create the mixture */
  mixture_args.metadata = metadata;
  mixture_args.lines = line_list;
  mixture_args.molecules[0].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[0].concentration = 1.0/3.0;
  mixture_args.molecules[0].cutoff = 25;
  mixture_args.molecules[0].id = 1; /* H2O */
  mixture_args.molecules[1].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[1].concentration = 1.0/3.0;
  mixture_args.molecules[1].cutoff = 50;
  mixture_args.molecules[1].id = 2; /* CO2 */
  mixture_args.molecules[2].nisotopes = 0; /* Handle all isotopes */
  mixture_args.molecules[2].concentration = 1.0/3.0;
  mixture_args.molecules[2].cutoff = 25;
  mixture_args.molecules[2].id = 3; /* O3 */
  mixture_args.nmolecules = 3;
  mixture_args.wavenumber_range[0] = 0;
  mixture_args.wavenumber_range[1] = INF;
  mixture_args.pressure = 1;
  mixture_args.temperature = 296;
  CHK(sln_mixture_create(sln, &mixture_args, &mixture) == RES_OK);

  test_tree(mixture, line_list);
  test_tree_serialization(sln, shtr, mixture);

  CHK(sln_device_ref_put(sln) == RES_OK);
  CHK(sln_mixture_ref_put(mixture) == RES_OK);
  CHK(shtr_ref_put(shtr) == RES_OK);
  CHK(shtr_line_list_ref_put(line_list) == RES_OK);
  CHK(shtr_isotope_metadata_ref_put(metadata) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}

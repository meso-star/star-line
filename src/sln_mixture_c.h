/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SLN_MIXTURE_C_H
#define SLN_MIXTURE_C_H

#include "sln.h"
#include "sln_line.h"

#include <rsys/dynamic_array.h>
#include <rsys/ref_count.h>

struct sln_device;
struct shtr_line_view;

/* Generate the dynamic array of lines */
#define DARRAY_DATA struct line
#define DARRAY_NAME line
#include <rsys/dynamic_array.h>

struct molecule_params {
  /* Map the isotope local identifier to its abundance */
  double isotopes_abundance[SLN_MAX_ISOTOPES_COUNT];

  double concentration;
  double cutoff;
};

/* Current version of the mixture. One should increment it and perform a
 * version management onto serialized data when muxture structure is updated. */
static const int SLN_MIXTURE_VERSION = 0;

struct sln_mixture {
  /* Map the molecule id to its parameters (i.e. concentration, cutoff,
   * isotopes abundance) */
  struct molecule_params molecules_params[SLN_MAX_MOLECULES_COUNT];

  struct shtr_isotope_metadata* metadata;
  struct shtr_line_view* line_view; /* Set of lines */

  /* Store per line precomputed data */
  struct darray_line lines;

  double temperature; /* In Kelvin */
  double pressure; /* In atm */
  double wavenumber_range[2]; /* Spectral range [cm^-1] */

  struct sln_device* sln;
  ref_T ref;
};

static INLINE const struct molecule_params*
mixture_get_molecule_params
  (const struct sln_mixture* mixture,
   const int32_t molecules_id)
{
  ASSERT(mixture && molecules_id >= 0 && molecules_id < SLN_MAX_MOLECULES_COUNT);
  return mixture->molecules_params + molecules_id;
}

#endif /* SLN_MIXTURE_C_H */

# Star-Line

Star-Line is a C library whose purpose is to manage spectral transitions
(lines).

Star-Line relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build. It also depends
on the [RSys](https://gitlab.com/vaplv/rsys/) and
[Star-HITRAN](https://gitlab.com/meso-star/star-hitran/) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the aforementioned prerequisites. Finally generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies. The
resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Copyright notice

Copyright (C) 2022 CNRS - LMD  
Copyright (C) 2022 [|Meso|Star>](https://www.meso-star.com) (<contact@meso-star.com>)  
Copyright (C) 2022 Université Paul Sabatier - IRIT  
Copyright (C) 2022 Université Paul Sabatier - Laplace

## License

Star-Line is free software released under the GPL v3+ license: GNU GPL version
3 or later. You are welcome to redistribute it under certain conditions; refer
to the COPYING file for details.
